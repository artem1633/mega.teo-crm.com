<?php

namespace app\modules\api\controllers;

use app\components\helpers\TelegramAPI;
use app\models\Account;
use app\models\AccountChanel;
use app\models\Key;
use app\models\KeyOne;
use app\models\KeyOneMinus;
use app\models\Log;
use app\models\Message;
use app\models\Request;
use app\models\Settings;
use app\models\TelegramChanel;
use app\models\TelegramChanelBlack;
use app\models\TelegramChanelKey;
use app\models\TelegramHistory;
use app\models\TelegramHistorySearch;
use app\models\TelegramPost;
use app\models\AdvertisingReport;
use app\models\TelegramUser;
use PHPUnit\Util\Blacklist;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;
use danog\MadelineProto\API;

class TelegramController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['bot-in','test','bot-update','send-refer','webhook','getDialog'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $MadelineProto = new API('session.madeline');

        $settings = array(
            'peer' => '@naudalenkebro', //название_канала, должно начинаться с @, например @breakingmash, все остальные параметры, кроме limit, можно оставить равными 0
            'offset_id' => 0,
            'offset_date' => 0,
            'add_offset' => 0,
            'limit' =>100, //Количество постов, которые вернет клие  нт
            'max_id' => 0, //Максимальный id поста
            'min_id' => 0, //Минимальный id поста - использую для пагинации, при  0 возвращаются последние посты.
            'hash' => 0
        );

        $data = $MadelineProto->get_full_info('@naudalenkebro');

        $filename = "data2.html";

        VarDumper::dump($data, 10, true);

//        $str = serialize($data);

//        file_put_contents( $filename , $str  );
    }

    /**
     * @param $type
     * @param $text
     * @param TelegramChanel $chanel
     */
    public  function actionGetJoin(){
        // echo $_SERVER['REMOTE_ADDR'];

        // exit;
        $accaunts = Account::find()->where(['check' => true, 'url_server' => $_SERVER['REMOTE_ADDR']])->limit(1)->all();
        /** @var Account $accaunt */
        foreach ($accaunts as $accaunt) {
            // var_dump($accaunt);

            // $time = date('Y-m-d H:i', strtotime('-'.$accaunt->interval.' minutes'));
            // if ($accaunt->send_time > $time and $accaunt->send_time) {
            //     continue;
            // }

            // $accaunt->count_send = $accaunt->count_send + $accaunt->count_send_time;
            // $accaunt->count_good = $accaunt->count_good + $accaunt->count_send_time;

            // if ($accaunt->count_send >= $accaunt->limit_24) {
            //     $accaunt->send_status = 0;
            // } else {
            //     $accaunt->send_status = 1;
            // }

            $accaunt->send_time = date('Y-m-d H:i:s');
            $accaunt->save(false);

            // $chanels = TelegramChanel::find()
            //     ->where(['is not','username',null])
            //     ->andWhere(['can_view_participants' => 2])
            //     ->all();
            // $ac = AccountChanel::findAll($accaunt->id);
            // if (count($chanels) <= count($ac)) {
            //     $accaunt->status = 'send';
            //     $accaunt->save(false);
            //     continue;
            // }

            $list['user'] = [
                'id' => $accaunt->id,
                'file_name' => $accaunt->file_name,
                'code_file' => $accaunt->code_file,
                'url_server' => $accaunt->url_server,
            ];
            $ib = 0;
            /** @var TelegramChanel $chanel */
            // foreach ($chanels as $chanel) {
            //     if ($ib >= $accaunt->count_send_time) {
            //         break;
            //     }
            //     $ib++;

            //     $list['list'][] = [
            //         'id' => $chanel->id,
            //         'username' => $chanel->username
            //     ];
            //     $chanel->send_bl = false;
            //     $chanel->join_id = $accaunt->id;
            //     (new AccountChanel([
            //         'chanel_id' => $chanel->id,
            //         'account_id' => $accaunt->id,
            //         'send_bl' => false,
            //         'count_send' => 0,
            //         'ban' => false,
            //         'status' => false,
            //     ]))->save(false);
            //     $chanel->save(false);
            // }
            return json_encode($list);
        }
    }

    /**
     * @param $type
     * @param $text
     * @param TelegramChanel $chanel
     */
    public  function actionGetInfo(){
        $accaunts = Account::find()->where(['check' => true, 'url_server' => $_SERVER['REMOTE_ADDR']])->all();
//        $accaunts = Account::find()->where(['id' => [17,43,29]])->limit(10)->orderBy('RAND()')->all();
//        $accaunts = Account::find()->where(['>','id',14])->limit(10)->orderBy('RAND()')->all();

        $list['list'] = [];
        /** @var Account $accaunt */
        foreach ($accaunts as $accaunt) {
            // var_dump($accaunt);

            $time = date('Y-m-d H:i', strtotime('-'.$accaunt->interval.' minutes'));
            if ($accaunt->send_time > $time and $accaunt->send_time) {
                continue;
            }


            $settingCount = Settings::findByKey('chanel_count_account')->value;
            $co = TelegramChanel::find()->where(['join_id' => $accaunt->id])->count();
            if ($co >= $settingCount) {
                continue;
            }
            $joinCount = Log::find()
                ->where(['like','datetime', date('Y-m-d')])
                ->andWhere(['account_id' => $accaunt->id, 'type' => 'join'])
                ->count();
            if ($joinCount >= 100) {
                continue;
            }
            // $accaunt->count_send = $accaunt->count_send + $accaunt->count_send_time;

            // if ($accaunt->count_send >= $accaunt->limit_24) {
            //     $accaunt->send_status = 0;
            // } else {
            //     $accaunt->send_status = 1;
            // }



            $chanels = TelegramChanel::find()
                ->where(['is','username',null])
                ->orWhere(['is','join_id', null])
                ->orderBy('RAND()')
                ->all();


            if (count($chanels) < 1){
                continue;
            }
            $ib = 0;
            $chAll = [];
            /** @var TelegramChanel $chanel */
            foreach ($chanels as $chanel) {
                if ($ib >= $accaunt->count_send_time) {
                    break;
                }
                $ib++;

                $chAll[] = [
                    'id' => $chanel->id,
                    'username' => $chanel->url
                ];
                $chanel->datetime_last_check = date('Y-m-d H:i');
                $chanel->username = $chanel->url;
                $chanel->send_bl = false;
                $chanel->join_id = $accaunt->id;
                $chanel->join_date = date('Y-m-d H:i');
                $chanel->save(false);
                $this->log('join','add', 'new', $chanel->id, $accaunt->id);
            }
            $accaunt->count_fuck = $accaunt->count_fuck + $ib;
            $list['list'][]['user'] = [
                'id' => $accaunt->id,
                'file_name' => $accaunt->file_name,
                'code_file' => $accaunt->code_file,
                'url_server' => $accaunt->url_server,
                'list' => $chAll,
            ];

            if ($chanels) {
                $this->log('info',serialize($chAll), 'start', null, $accaunt->id);
                $accaunt->send_time = date('Y-m-d H:i:s');
                $accaunt->save(false);
            } else {
                continue ;
            }
        }
        return json_encode($list);
    }


    /**
     * @param $type
     * @param $text
     * @param TelegramChanel $chanel
     */
    public  function actionSetInfo(){

        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;


        $result = $request->post();

        $chanel = TelegramChanel::findOne($result['chanel_id']);

        if (!$chanel){
            return 'проблемы с каналом';
        }
        $accaunt = Account::findOne($result['account_id']);
        if ($accaunt) {
            $accaunt->count_fuck = $accaunt->count_fuck - 1;
            $accaunt->count_good = $accaunt->count_good + 1;
            $accaunt->save(false);
        }

        if (isset($result['username']) == false) {
            $this->log('info','error1', serialize($result), $chanel->id);
            $chanel->name = $chanel->url;
        }

        if (isset($result['title'])) {
            $chanel->name = $this->remove_emoji($result['title']);
        } else {
            $chanel->name = $chanel->url;
        }
        if (isset($result['about'])) {
            $chanel->description = $this->remove_emoji($result['about']);
        }
        if (isset($result['participants_count'])) {
            $chanel->subscribers_count = $result['participants_count'];
        }
        if (isset($result['username'])) {
            $chanel->username = $result['username'];
        }
        if (isset($result['photo'])) {
            $filename = uniqid();
            $filename2 = md5(microtime() . rand(0, 9999));
            $name = $filename.'_'.$filename2 . '.jpg';
            $url = $result['photo'];
            $path = $_SERVER['DOCUMENT_ROOT'] . '/avatars/'.$name.'.jpg';
            file_put_contents($path, file_get_contents($url));
            $chanel->img = 'http://'.$_SERVER['SERVER_ADDR'].'/avatars/'.$name.'.jpg';
        }
        if (isset($result['can_view_participants'])) {
            $chanel->can_view_participants = $result['can_view_participants'];
        }
        if (isset($result['type'])) {
            $chanel->type = $result['type'];
        }

        $chanel->datetime_last_check = date('Y-m-d H:i');
        $settingTime = Settings::findByKey('time_post')->value;
        $chanel->send_time = date('Y-m-d H:i', strtotime('-'.$settingTime.' minutes'));;
        $chanel->save(false);



        $date = date('Y-m-d H:i');

        $his = new TelegramHistory([
            'chanel_id' => $chanel->id,
            'subscribers_count' => $chanel->subscribers_count,
            'posts_count' => 0,
            'created_at' => $date,
        ]);
        $his->save(false);

    }

    /**
     * @param $type
     * @param $text
     * @param TelegramChanel $chanel
     */
    public  function actionGetReport(){
        ini_set('memory_limit', '2000M');
        set_time_limit(60);
        $lim = 0;
        $accaunts = Account::find()->where(['check' => true, 'url_server' => $_SERVER['REMOTE_ADDR']])
//            ->orderBy('RAND()')
            ->all();
//        $accaunts = Account::find()->where(['id' => 21])->limit(10)->all();
        $list['list'] = [];
        /** @var Account $accaunt */
        foreach ($accaunts as $accaunt) {
            // var_dump($accaunt);


//            if ($_SERVER['REMOTE_ADDR'] != '194.67.78.11') {
            $time = date('Y-m-d H:i', strtotime('-'.$accaunt->interval.' minutes'));
            if ($accaunt->send_time > $time and $accaunt->send_time) {
                continue;
            }
//            }

            $lim++;
//            if ($lim == 10){
//                break;
//            }


            $settingInterval = Settings::findByKey('interval_chanel')->value;
            $settingCount = Settings::findByKey('chanel_count')->value;

            $time = date('Y-m-d H:i', strtotime('-'.$settingInterval.' minutes'));

            $chanels = TelegramChanel::find()->where(['<','datetime_last_check', $time])
                ->andWhere(['is not','username',null])
                ->andWhere(['join_id' => $accaunt->id])
                ->andWhere(['ban' => false])
//                ->orWhere(['is','datetime_last_check', null])
//                ->limit($accaunt->count_send_time)
                ->orderBy(['datetime_last_check' => SORT_ASC])
                ->all();
            if (count($chanels) < 1){
                continue;
            }
            $ib = 0;
            $chAll = [];
            /** @var TelegramChanel $chanel */
            foreach ($chanels as $chanel) {
                if ($ib >= $accaunt->count_send_time) {
                    break;
                }
                $ib++;

                $chAll[] = [
                    'id' => $chanel->id,
                    'username' => $chanel->url,
                    'img' => $chanel->img
                ];
                $chanel->send_bl = false;
                $chanel->join_id = $accaunt->id;
                $chanel->save(false);
            }
            $accaunt->count_fuck = $accaunt->count_fuck + $ib;


            $list['list'][]['user'] = [
                'id' => $accaunt->id,
                'file_name' => $accaunt->file_name,
                'code_file' => $accaunt->code_file,
                'url_server' => $accaunt->url_server,
                'list' => $chAll,
            ];
            if ($chanels){
                $accaunt->send_time = date('Y-m-d H:i:s');
                $accaunt->save(false);
                $this->log('report',serialize($chAll), 'start', null, $accaunt->id);
            } else {
                continue ;
            }
        }
        return json_encode($list);
    }


    /**
     * @param $type
     * @param $text
     * @param TelegramChanel $chanel
     */
    public  function actionSetReport(){

        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;


        $result = $request->post();

        $chanel = TelegramChanel::findOne($result['chanel_id']);

        if (!$chanel){
            return 'проблемы с каналом';
        }

        $accaunt = Account::findOne($result['account_id']);
        if ($accaunt) {
            $accaunt->count_fuck = $accaunt->count_fuck - 1;
            $accaunt->count_good = $accaunt->count_good + 1;
            $accaunt->save(false);
        }


        if (isset($result['username']) == false) {
            $this->log('report','error1', serialize($result), $chanel->id);
            $chanel->name = $chanel->url;
        }

        if (isset($result['title'])) {
            $chanel->name = $this->remove_emoji($result['title']);
        }
        if (isset($result['about'])) {
            $chanel->description = $this->remove_emoji($result['about']);
        }
        if (isset($result['participants_count'])) {
            $chanel->subscribers_count = $result['participants_count'];
        }
        if (isset($result['username'])) {
            $chanel->username = $result['username'];
        }
        if (isset($result['photo'])) {
            if (!$chanel->img){
                $filename = uniqid();
                $filename2 = md5(microtime() . rand(0, 9999));
                $name = $filename.'_'.$filename2 . '.jpg';
                $url = $result['photo'];
                $path = $_SERVER['DOCUMENT_ROOT'] . '/avatars/'.$name.'.jpg';
                file_put_contents($path, file_get_contents($url));
                $chanel->img = 'http://'.$_SERVER['SERVER_ADDR'].'/avatars/'.$name.'.jpg';
            }
        }
        if (isset($result['can_view_participants'])) {
            $chanel->can_view_participants = $result['can_view_participants'];
        }
        if (isset($result['type'])) {
            $chanel->type = $result['type'];
        }



        $hisOld = TelegramHistory::find()->where(['chanel_id' => $chanel->id])->orderBy(['id' => SORT_DESC])->one();
        $grown = 0;
        if ($hisOld){
            $grown = $result['participants_count'] - $hisOld->subscribers_count;
        }
        $chanel->datetime_last_check = date('Y-m-d H:i');
        $chanel->save(false);
        $date = date('Y-m-d H:i');

        $his = new TelegramHistory([
            'chanel_id' => $chanel->id,
            'subscribers_count' => $result['participants_count'],
            'grown' => $grown,
            'posts_count' => 0,
            'created_at' => $date,
        ]);
        $his->save(false);

    }

    /**
     * @param $type
     * @param $text
     * @param TelegramChanel $chanel
     */
    public  function actionSetReport2(){

        Yii::$app->response->format = Response::FORMAT_JSON;
//        $request = Yii::$app->request;


//        $result2 = $request->post();
        $postData = file_get_contents('php://input');
//        var_dump($postData);
//        $serial= $result2['serial'];
//        $this->log('info','error1', $serial);
        $list = json_decode($postData, true);
//        var_dump($list);
//        exit();
//        var_dump($serial);
        foreach ($list['list'] as $result) {

//            var_dump($result);
//            exit;
            $chanel = TelegramChanel::findOne($result['chanel_id']);

            if (!$chanel){
                return 'проблемы с каналом';
            }

            $accaunt = Account::findOne($result['account_id']);
            if ($accaunt) {
                $accaunt->count_fuck = $accaunt->count_fuck - 1;
                $accaunt->count_good = $accaunt->count_good + 1;
                $accaunt->save(false);
            }


            if (isset($result['username']) == false) {
                $chanel->name = $chanel->url;
            }

            if (isset($result['title'])) {
                $chanel->name = $this->remove_emoji($result['title']);
            }
            if (isset($result['about'])) {
                $chanel->description = $this->remove_emoji($result['about']);
            }
            if (isset($result['participants_count'])) {
                $chanel->subscribers_count = $result['participants_count'];
            }
            if (isset($result['username'])) {
                $chanel->username = $result['username'];
            }
            if (isset($result['photo'])) {
                if (!$chanel->img){
                    $filename = uniqid();
                    $filename2 = md5(microtime() . rand(0, 9999));
                    $name = $filename.'_'.$filename2 . '.jpg';
                    $url = $result['photo'];
                    $path = $_SERVER['DOCUMENT_ROOT'] . '/avatars/'.$name.'.jpg';
                    file_put_contents($path, file_get_contents($url));
                    $chanel->img = 'http://'.$_SERVER['SERVER_ADDR'].'/avatars/'.$name.'.jpg';
                }
            }
            if (isset($result['can_view_participants'])) {
                $chanel->can_view_participants = $result['can_view_participants'];
            }
            if (isset($result['type'])) {
                $chanel->type = $result['type'];
            }



            $hisOld = TelegramHistory::find()->where(['chanel_id' => $chanel->id])->orderBy(['id' => SORT_DESC])->one();
            $grown = 0;
            if ($hisOld){
                $grown = $result['participants_count'] - $hisOld->subscribers_count;
            }
            $chanel->datetime_last_check = date('Y-m-d H:i');
            $chanel->save(false);
            $date = date('Y-m-d H:i');

            $his = new TelegramHistory([
                'chanel_id' => $chanel->id,
                'subscribers_count' => $result['participants_count'],
                'grown' => $grown,
                'posts_count' => 0,
                'created_at' => $date,
            ]);
            $his->save(false);
            $this->log('report','no error', $result['participants_count'], $chanel->id, $accaunt->id);
        }

    }

    /**
     * @param $type
     * @param $text
     * @param TelegramChanel $chanel
     */
    public  function actionGetPost(){
        ini_set('memory_limit', '2000M');
        set_time_limit(60);

        $accaunts = Account::find()->where(['check' => true, 'url_server' => $_SERVER['REMOTE_ADDR']])->limit(10)->all();
        /** @var Account $accaunt */
        foreach ($accaunts as $accaunt) {
            // var_dump($accaunt);

//            $time = date('Y-m-d H:i', strtotime('-'.$accaunt->interval.' minutes'));
//            if ($accaunt->send_time > $time and $accaunt->send_time) {
//                continue;
//            }


            $accaunt->count_fuck = $accaunt->count_fuck + $accaunt->count_send_time;

            $list['user'] = [
                'id' => $accaunt->id,
                'file_name' => $accaunt->file_name,
                'code_file' => $accaunt->code_file,
                'url_server' => $accaunt->url_server,
            ];

            $settingInterval = Settings::findByKey('time_post')->value;

            $time = date('Y-m-d H:i', strtotime('-'.$settingInterval.' minutes'));

            $chanels = TelegramChanel::find()->where(['<','send_time', $time])
                ->andWhere(['is not','username',null])
                ->andWhere(['join_id' => $accaunt->id])
//                ->orWhere(['is','datetime_last_check', null])
//                ->limit($accaunt->count_send_time)
                ->orderBy(['send_time' => SORT_ASC])
                ->all();

            $ib = 0;
            /** @var TelegramChanel $chanel */
            foreach ($chanels as $chanel) {
                if ($ib >= $accaunt->count_send_time) {
                    break;
                }
                $ib++;

                $list['list'][] = [
                    'id' => $chanel->id,
                    'username' => $chanel->url,
                    'limit' => 100,
                    'min_id' => $chanel->last_post_id
                ];
                $chanel->save(false);
            }
            if ($chanels){
                $this->log('post',serialize($list), 'start', null, $accaunt->id);
                return json_encode($list);
            } else {
                continue ;
            }
        }
        return '';
    }


    /**
     * @param $type
     * @param $text
     * @param TelegramChanel $chanel
     */
    public  function actionSetPost(){

        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;


        $result = $request->post();

        $chanel = false;
        if (isset($result['chanel_id'])){
            $chanel = TelegramChanel::findOne($result['chanel_id']);
        }
        if (!$chanel){
            return 'проблемы с каналом';
        }

        $accaunt = Account::findOne($result['account_id']);
        if ($accaunt) {
            $accaunt->count_fuck = $accaunt->count_fuck - 1;
            $accaunt->count_good = $accaunt->count_good + 1;
            $accaunt->save(false);
        }

        $list = unserialize($result['list']);
//        echo $list;
//        exit;
        if ($list) {
            foreach ($list as $item) {
//                var_dump($item);
//                exit;
                $message = $item['message'];

                $c = Message::find()->where(['text_id' => $item['id'], 'chanel_id' => $chanel->id])->one();
                if ($c){
                    continue;
                }
                $message = mb_strtolower($message);
                $message = $this->remove_emoji($message);
                $message = preg_replace('/\pP/iu', ' ', $message);

                (new Message([
                    'chanel_id' => $chanel->id,
                    'text' => $message,
                    'text_id' => $item['id'],
                    'datetime' => date('Y-m-d H:i'),
                ]))->save(false);
            }
        }
        $chanel->last_post_id = $list[0]['id'];
        $chanel->last_post_text = $list[0]['message'];
        $chanel->send_time = date('Y-m-d H:i');
        $chanel->save(false);

    }


    /**
     * @param $type
     * @param $text
     * @param TelegramChanel $chanel
     */
    public  function actionLog(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;


        $result = $request->post();
        $this->log($result['type'],$result['text'], $result['info'], $result['chanel_id'], $result['account_id']);
    }

    /**
     * @param $type
     * @param $text
     * @param TelegramChanel $chanel
     */
    public  function actionClearAccInfo(){
        $accaunts = Account::find()->where(['check' => true])->all();
        /** @var Account $accaunt */
        foreach ($accaunts as $accaunt) {
            $accaunt->count_good = 0;
            $accaunt->count_fuck = 0;
            $accaunt->save(false);
        }
        Log::deleteAll(['<>','type','join']);
    }

    /**
     * @param $type
     * @param $text
     * @param TelegramChanel $chanel
     */
    public  function actionBanChanel(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $result = $request->post();

        $chanel = TelegramChanel::findOne($result['chanel_id']);
        $log =  Log::find()
            ->andWhere(['chanel_id' => $chanel->id, 'type' => 'join'])
            ->one();
        if ($log) {
            $log->delete();
        }
        $chanel->ban = true;
        $chanel->ban_date = date('Y-m-d H:i');
        if (isset($result['text'])) {
            $chanel->ban_info = $result['text'];
        }
        $chanel->save(false);

        $this->log($result['type'],$result['text'], $result['info'], $result['chanel_id'], $result['account_id']);
    }

    /**
     * @param $type
     * @param $text
     * @param TelegramChanel $chanel
     */
    public  function actionBanAcc(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $result = $request->post();

        $acc = Account::findOne($result['account_id']);
        $acc->check = false;
        $acc->save(false);

        $this->log($result['type'],$result['text'], $result['info'], $result['chanel_id'], $result['account_id']);
    }

    /**
     * @param $type
     * @param $text
     * @param TelegramChanel $chanel
     */
    public  function actionBanAccOff(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $result = $request->post();

        $acc = Account::findOne($result['account_id']);
        $acc->check = true;
        $acc->save(false);

        $this->log($result['type'],$result['text'], $result['info'], $result['chanel_id'], $result['account_id']);
    }

    public function actionGetNewPosts()
    {
        ini_set('memory_limit', '2000M');
        set_time_limit(60);
        $settingInterval = Settings::findByKey('interval_chanel')->value;
        $settingCount = Settings::findByKey('chanel_count')->value;

        $time = date('Y-m-d H:i', strtotime('-'.$settingInterval.' minutes'));

        $chanels = TelegramChanel::find()->where(['<','datetime_last_check', $time])
            ->orWhere(['is','datetime_last_check',null])
            ->limit($settingCount)
            ->orderBy(['datetime_last_check' => SORT_ASC])
            ->all();

//        $chanels = TelegramChanel::find()->where(['id' => 2599])->all();
        /** @var TelegramChanel $chanel */
        foreach ($chanels as $chanel)
        {

            $min_id = 0;
            if ($chanel->last_post_id) {
                $min_id = $chanel->last_post_id;
            }

            var_dump($chanel->id);
            $obj = $this->getInfo('get-post.php', $chanel->username);

            if (!$obj){
                var_dump('bbb');
                $chanel->datetime_last_check = date('Y-m-d H:i');
                $chanel->save(false);
                continue;
            }
//            var_dump($obj);
//            exit();
            if (is_array($obj) == false){
                var_dump($obj);
                $this->log('get-post.php','error3', $obj);
                continue;
            }
            foreach ($obj as $item) {

                $message = $item['message'];

                $c = Message::find()->where(['text_id' => $item['id'], 'chanel_id' => $chanel->id])->one();
                if ($c){
                    continue;
                }
                $message = mb_strtolower($message);
                $message = $this->remove_emoji($message);
                $message = preg_replace('/\pP/iu', ' ', $message);

                (new Message([
                    'chanel_id' => $chanel->id,
                    'text' => $message,
                    'text_id' => $item['id'],
                    'datetime' => date('Y-m-d H:i'),
                ]))->save(false);
                $pks = TelegramChanelKey::find()->where(['telegram_chanel_id' => $chanel->id])->all();

                foreach ($pks as $item3) {

                    $keyGood = KeyOne::find()->where(['key_id' => $item3->key_id])->all();
                    $keyFuck = ArrayHelper::getColumn(KeyOneMinus::find()->where(['key_id' => $item3->key_id])->all(), 'value');

                    $searchGood = false;
                    $searchFuck = false;
                    $wordGood = '';
                    $wordGoodId = null;
                    $wordFuck = '';
                    /** @var KeyOne $item */
                    foreach ($keyGood as $item11) {
                        $searWord = mb_strtolower($item11->value);
                        if (strpos($message, $searWord) !== false) {
                            $wordGood = $item11->value;
                            $wordGoodId = $item11->id;
                            $searchGood = true;
                            break;
                        }
                    }

                    foreach ($keyFuck as $item22) {
                        $item22 = mb_strtolower($item22);
                        if (strpos($message, $item22) !== false) {
                            $wordFuck = $item22;
                            $searchFuck = true;
                            break;
                        }
                    }

                    if ($searchFuck == false and $searchGood == true) {
                        $key = Key::findOne($item3->key_id);
                        $black = TelegramChanelBlack::find()
                            ->where(['telegram_chanel_id' => $chanel->id, 'key_id' => $key->id])
                            ->one();
                        if (!$black) {
                            $request = new Request([
                                'telegram_chanel_id' => $chanel->id,
                                'text' => $message,
                                'status' => $wordGood,
                                'key_one_id' => $wordGoodId,
                                'company_id' => $key->company_id,
                            ]);
                            $request->save(false);
                        }
                    }
                }
                $pks = Key::find()->where(['common_base_searchable' => true])->all();

                foreach ($pks as $item4) {

                    $keyGood = KeyOne::find()->where(['key_id' => $item4->id])->all();
                    $keyFuck = ArrayHelper::getColumn(KeyOneMinus::find()->where(['key_id' => $item4->id])->all(), 'value');

                    $searchGood = false;
                    $searchFuck = false;
                    $wordGood = '';
                    $wordGoodId = null;
                    $wordFuck = '';
                    /** @var KeyOne $item */
                    foreach ($keyGood as $item11) {
                        $searWord = mb_strtolower($item11->value);
                        if (strpos($message, $searWord) !== false) {
                            $wordGood = $item11->value;
                            $wordGoodId = $item11->id;
                            $searchGood = true;
                            break;
                        }
                    }


                    foreach ($keyFuck as $item22) {
                        $item22 = mb_strtolower($item22);
                        if (strpos($message, $item22) !== false) {
                            $wordFuck = $item22;
                            $searchFuck = true;
                            break;
                        }
                    }


                    if ($searchFuck == false and $searchGood == true) {
                        $black = TelegramChanelBlack::find()
                            ->where(['telegram_chanel_id' => $chanel->id, 'key_id' => $item4->id])
                            ->one();
                        if (!$black) {
                            $request = new Request([
                                'telegram_chanel_id' => $chanel->id,
                                'text' => $message,
                                'status' => $wordGood,
                                'key_one_id' => $wordGoodId,
                                'company_id' => $item4->company_id,
                            ]);
                            $request->save(false);
                        }

                    }
                }

            }

            $date = date('Y-m-d');
            $his = TelegramHistory::find()->where(['created_at' => $date, 'chanel_id' => $chanel->id])->one();
            var_dump('aaa');
            if ($his) {
                $postCoint = $obj[0]['id'] - $his->last_post_id;
                $chanel->count_posts_today = $postCoint;
                var_dump($postCoint);
                $chanel->save(false);
            } else {
                $chanel->count_posts_today = 0;
            }

            $chanel->datetime_last_check = date('Y-m-d H:i');
            $chanel->last_post_id = $obj[0]['id'];
            $chanel->last_post_text = $obj[0]['message'];
            $chanel->save(false);
        }

        echo 'success';
    }

    public function actionChanelReport()
    {

        $chanels = TelegramChanel::find()->where(['is not','username',null])->all();
//        $chanels = TelegramChanel::find()->where(['=','id',17])->all();
        var_dump(count($chanels));
        $i = 0;
        $i2 = 0;
        /** @var TelegramChanel $chanel */
        foreach ($chanels as $chanel)
        {
            $i2++;
            $dateLast = date('Y-m-d');
            $hisLast2 = TelegramHistory::find()
                ->where(['created_at' => $dateLast, 'chanel_id' => $chanel->id])
                ->one();


            if ($hisLast2) {
                continue;
            }

            echo "<br> {$i2}";
//            var_dump($hisLast2->created_at);

            $i++;
            if ($i == 20) {
                break;
            }

            $result = $this->getInfo('info.php', $chanel->username);

            if (isset($result['title'])) {
                $chanel->name = $this->remove_emoji($result['title']);
            } else {
                $chanel->name = $chanel->url;
            }
            if (isset($result['about'])) {
                $chanel->description = $this->remove_emoji($result['about']);
            }
            if (isset($result['participants_count'])) {
                $chanel->subscribers_count = $result['participants_count'];
            }
            if (isset($result['username'])  and $chanel->username != $result['username']) {
                $chanel->username = $result['username'];
            }
            if (isset($result['photo'])) {
                $chanel->img = $result['photo'];
            }
            if (isset($result['can_view_participants'])) {
                if ($chanel->can_view_participants != $result['can_view_participants'] and $chanel->can_view_participants != 2) {
                    $chanel->can_view_participants = $result['can_view_participants'];
                }
            }
            if (isset($result['type'])) {
                if ($chanel->type != $result['type']) {
                    $chanel->type = $result['type'];
                }
            }
            $chanel->save(false);

            $date = date('Y-m-d');
            $his = TelegramHistory::find()->where(['created_at' => $date, 'chanel_id' => $chanel->id])->one();

            $postCoint = 0;
            if (!$his) {
                /* нужно взять айди посленего поста и получить айди вчерашнего поста и отнять друг от друга это и есть кол-во постов сегодня*/

                $dateLast = date('Y-m-d', time() - 86400);
                $hisLast = TelegramHistory::find()->where(['created_at' => $dateLast, 'chanel_id' => $chanel->id])->one();
                if ($hisLast){
                    $postCoint = $chanel->last_post_id - $hisLast->last_post_id;
                }
                $his = new TelegramHistory([
                    'chanel_id' => $chanel->id,
                    'subscribers_count' => $chanel->subscribers_count,
                    'posts_count' => $postCoint,
                    'last_post_id' => $chanel->last_post_id,
//                    'company_id' => $chanel->company_id,
                    'created_at' => $date,
                ]);
                $his->save(false);
            } else {
                if (!$his->subscribers_count) {
                    $his->subscribers_count = $chanel->subscribers_count;
                    $his->save(false);
                }
            }
        }
    }

    public function actionChanelInfo()
    {
        $chanels = TelegramChanel::find()
            ->where(['is','name',null])
            ->orderBy('RAND()')
            ->limit(10)
            ->all();
        $i = 0;
        /** @var TelegramChanel $chanel */
        foreach ($chanels as $chanel)
        {

//            if (strpos($chanel->url, 'bot') == false) {

            $result = $this->getInfo('info.php', $chanel->url);

            if (isset($result['username']) == false) {
                $this->log('info','error3', serialize($result), $chanel->id);
                $chanel->name = $chanel->url;
                $chanel->save(false);
                continue;
            }
            $this->log('info','good3', null, $chanel->id);


            if (isset($result['title'])) {
                $chanel->name = $this->remove_emoji($result['title']);
            } else {
                $chanel->name = $chanel->url;
            }
            if (isset($result['about'])) {
                $chanel->description = $this->remove_emoji($result['about']);
            }
            if (isset($result['participants_count'])) {
                $chanel->subscribers_count = $result['participants_count'];
            }
            if (isset($result['username'])) {
                $chanel->username = $result['username'];
            }
            if (isset($result['photo'])) {
                $chanel->img = $result['photo'];
            }
            if (isset($result['can_view_participants'])) {
                $chanel->can_view_participants = $result['can_view_participants'];
            }
            if (isset($result['type'])) {
                $chanel->type = $result['type'];
            }
            $chanel->save(false);

            $date = date('Y-m-d');
            $his = TelegramHistory::find()->where(['created_at' => $date, 'chanel_id' => $chanel->id])->one();

            if (!$his) {
                $his = new TelegramHistory([
                    'chanel_id' => $chanel->id,
                    'subscribers_count' => $chanel->subscribers_count,
                    'posts_count' => 0,
//                        'company_id' => $chanel->company_id,
                    'created_at' => $date,
                ]);
                $his->save(false);
            } else {
                $his->subscribers_count = $chanel->subscribers_count;
                $his->save(false);
            }

//            }else {
//                $chanel->name = $chanel->url;
//                $chanel->save(false);
//            }
        }
    }
    public function actionListUsers()
    {
        $chanels = TelegramChanel::find()->where(['can_view_participants' => 1])->limit(20)->all();
        /** @var TelegramChanel $chanel */
        foreach ($chanels as $chanel)
        {
            $result = $this->getInfo('info_user.php', $chanel->username);
//            var_dump($result);
//            exit;
            if (!$result) {
                $this->log('list','error4', null,  $chanel->id);
                break;
            }
            $i = 0;
            if (!$result){
                continue;
            }
            foreach ($result as $item) {
                $i++;
                $user = TelegramUser::find()->where(['login' => $item['username'], 'chanel_id' => $chanel->id])->one();
                if (!$user) {
                    $phone = '';
                    if (isset($item['phone'])){
                        $phone = $item['phone'];
                    }
                    $user = new TelegramUser([
                        'chanel_id' => $chanel->id,
                        'name' => $this->remove_emoji($item['name'] . ' '. $item['last_name']),
                        'login' => $item['username'],
//                        'img' => 'Превью',
//                        'phone' => $phone,
//                        'description' => $item[''],
//                        'company_id' => $chanel->company_id,
                        'is_new' => true,
                        'updated_at' => date('Y-m-d H:m'),
                        'created_at' => date('Y-m-d H:m'),
                    ]);
                    $user->save(false);
                }
            }

            $chanel->can_view_participants = 2;
            $chanel->save(false);
            var_dump($i);
        }
    }
    public function actionListUsers2()
    {
        $users = TelegramUser::find()->where(['is_new' => true])->andWhere(['is not','login',null])->limit(10)->all();
        /** @var TelegramUser $user */
        foreach ($users as $user)
        {

            $result = $this->getInfo('info_user2.php', $user->login);

            if (isset($result['phone'])) {
                $user->phone = $result['phone'];
            }

            if (isset($result['photo'])) {
                $user->img = $result['photo'];
            }

            if (isset($result['about'])) {
                $user->description = $this->remove_emoji($result['about']);
            }

            $user->is_new = false;
            $user->save(false);

        }
    }
    public function actionClear()
    {
        $users = TelegramChanel::find()->all();
        /** @var TelegramUser $user */
        foreach ($users as $user)
        {
            $del = TelegramChanel::find()->where(['<>', 'id', $user->id])
                ->andWhere(['name' => $user->name])->one();

            if ($del) var_dump($del->name);
            if ($del){
                $del->delete();
            }

//            $del2 = TelegramChanel::find()->where(['!=', 'id', $user->id])
//                ->andWhere(['username' => $user->username])->one();
//            if ($del2) $del2->delete();

        }

    }
    public function actionClear2()
    {
        $users = TelegramChanel::find()->where(['is','username',null])->all();
        /** @var TelegramUser $user */
        foreach ($users as $user)
        {
//            $del = TelegramChanel::find()->where(['<>', 'id', $user->id])
//                ->andWhere(['name' => $user->name])->one();
//
//            if ($del){
            $user->delete();
//            }

//            $del2 = TelegramChanel::find()->where(['!=', 'id', $user->id])
//                ->andWhere(['username' => $user->username])->one();
//            if ($del2) $del2->delete();

        }

    }

    public function setMessage($username = null, $message = null)
    {
        $account = Account::find()->orderBy(['check' => SORT_DESC])->one();
        $response = file_get_contents($account->url_server.'send.php?session='.$account->file_name.'&peer='.$username.'&message='.$message);
        return true;
    }

    public static function remove_emoji($text){
//        $text = preg_replace('/[\x{1F3F4}](?:\x{E0067}\x{E0062}\x{E0077}\x{E006C}\x{E0073}\x{E007F})|[\x{1F3F4}](?:\x{E0067}\x{E0062}\x{E0073}\x{E0063}\x{E0074}\x{E007F})|[\x{1F3F4}](?:\x{E0067}\x{E0062}\x{E0065}\x{E006E}\x{E0067}\x{E007F})|[\x{1F3F4}](?:\x{200D}\x{2620}\x{FE0F})|[\x{1F3F3}](?:\x{FE0F}\x{200D}\x{1F308})|[\x{0023}\x{002A}\x{0030}\x{0031}\x{0032}\x{0033}\x{0034}\x{0035}\x{0036}\x{0037}\x{0038}\x{0039}](?:\x{FE0F}\x{20E3})|[\x{1F441}](?:\x{FE0F}\x{200D}\x{1F5E8}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F467}\x{200D}\x{1F467})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F467}\x{200D}\x{1F466})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F467})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F466}\x{200D}\x{1F466})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F466})|[\x{1F468}](?:\x{200D}\x{1F468}\x{200D}\x{1F467}\x{200D}\x{1F467})|[\x{1F468}](?:\x{200D}\x{1F468}\x{200D}\x{1F466}\x{200D}\x{1F466})|[\x{1F468}](?:\x{200D}\x{1F468}\x{200D}\x{1F467}\x{200D}\x{1F466})|[\x{1F468}](?:\x{200D}\x{1F468}\x{200D}\x{1F467})|[\x{1F468}](?:\x{200D}\x{1F468}\x{200D}\x{1F466})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F469}\x{200D}\x{1F467}\x{200D}\x{1F467})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F469}\x{200D}\x{1F466}\x{200D}\x{1F466})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F469}\x{200D}\x{1F467}\x{200D}\x{1F466})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F469}\x{200D}\x{1F467})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F469}\x{200D}\x{1F466})|[\x{1F469}](?:\x{200D}\x{2764}\x{FE0F}\x{200D}\x{1F469})|[\x{1F469}\x{1F468}](?:\x{200D}\x{2764}\x{FE0F}\x{200D}\x{1F468})|[\x{1F469}](?:\x{200D}\x{2764}\x{FE0F}\x{200D}\x{1F48B}\x{200D}\x{1F469})|[\x{1F469}\x{1F468}](?:\x{200D}\x{2764}\x{FE0F}\x{200D}\x{1F48B}\x{200D}\x{1F468})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F9B0})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F9B0})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F9B0})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F9B0})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F9B0})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F9B0})|[\x{1F575}\x{1F3CC}\x{26F9}\x{1F3CB}](?:\x{FE0F}\x{200D}\x{2640}\x{FE0F})|[\x{1F575}\x{1F3CC}\x{26F9}\x{1F3CB}](?:\x{FE0F}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FF}\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FE}\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FD}\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FC}\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FB}\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F9B8}\x{1F9B9}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F9DE}\x{1F9DF}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F46F}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93C}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FF}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FE}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FD}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FC}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FB}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F9B8}\x{1F9B9}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F9DE}\x{1F9DF}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F46F}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93C}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{200D}\x{2642}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{2695}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{2695}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{2695}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{2695}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{2695}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{200D}\x{2695}\x{FE0F})|[\x{1F476}\x{1F9D2}\x{1F466}\x{1F467}\x{1F9D1}\x{1F468}\x{1F469}\x{1F9D3}\x{1F474}\x{1F475}\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F934}\x{1F478}\x{1F473}\x{1F472}\x{1F9D5}\x{1F9D4}\x{1F471}\x{1F935}\x{1F470}\x{1F930}\x{1F931}\x{1F47C}\x{1F385}\x{1F936}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F483}\x{1F57A}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F6C0}\x{1F6CC}\x{1F574}\x{1F3C7}\x{1F3C2}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}\x{1F933}\x{1F4AA}\x{1F9B5}\x{1F9B6}\x{1F448}\x{1F449}\x{261D}\x{1F446}\x{1F595}\x{1F447}\x{270C}\x{1F91E}\x{1F596}\x{1F918}\x{1F919}\x{1F590}\x{270B}\x{1F44C}\x{1F44D}\x{1F44E}\x{270A}\x{1F44A}\x{1F91B}\x{1F91C}\x{1F91A}\x{1F44B}\x{1F91F}\x{270D}\x{1F44F}\x{1F450}\x{1F64C}\x{1F932}\x{1F64F}\x{1F485}\x{1F442}\x{1F443}](?:\x{1F3FF})|[\x{1F476}\x{1F9D2}\x{1F466}\x{1F467}\x{1F9D1}\x{1F468}\x{1F469}\x{1F9D3}\x{1F474}\x{1F475}\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F934}\x{1F478}\x{1F473}\x{1F472}\x{1F9D5}\x{1F9D4}\x{1F471}\x{1F935}\x{1F470}\x{1F930}\x{1F931}\x{1F47C}\x{1F385}\x{1F936}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F483}\x{1F57A}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F6C0}\x{1F6CC}\x{1F574}\x{1F3C7}\x{1F3C2}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}\x{1F933}\x{1F4AA}\x{1F9B5}\x{1F9B6}\x{1F448}\x{1F449}\x{261D}\x{1F446}\x{1F595}\x{1F447}\x{270C}\x{1F91E}\x{1F596}\x{1F918}\x{1F919}\x{1F590}\x{270B}\x{1F44C}\x{1F44D}\x{1F44E}\x{270A}\x{1F44A}\x{1F91B}\x{1F91C}\x{1F91A}\x{1F44B}\x{1F91F}\x{270D}\x{1F44F}\x{1F450}\x{1F64C}\x{1F932}\x{1F64F}\x{1F485}\x{1F442}\x{1F443}](?:\x{1F3FE})|[\x{1F476}\x{1F9D2}\x{1F466}\x{1F467}\x{1F9D1}\x{1F468}\x{1F469}\x{1F9D3}\x{1F474}\x{1F475}\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F934}\x{1F478}\x{1F473}\x{1F472}\x{1F9D5}\x{1F9D4}\x{1F471}\x{1F935}\x{1F470}\x{1F930}\x{1F931}\x{1F47C}\x{1F385}\x{1F936}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F483}\x{1F57A}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F6C0}\x{1F6CC}\x{1F574}\x{1F3C7}\x{1F3C2}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}\x{1F933}\x{1F4AA}\x{1F9B5}\x{1F9B6}\x{1F448}\x{1F449}\x{261D}\x{1F446}\x{1F595}\x{1F447}\x{270C}\x{1F91E}\x{1F596}\x{1F918}\x{1F919}\x{1F590}\x{270B}\x{1F44C}\x{1F44D}\x{1F44E}\x{270A}\x{1F44A}\x{1F91B}\x{1F91C}\x{1F91A}\x{1F44B}\x{1F91F}\x{270D}\x{1F44F}\x{1F450}\x{1F64C}\x{1F932}\x{1F64F}\x{1F485}\x{1F442}\x{1F443}](?:\x{1F3FD})|[\x{1F476}\x{1F9D2}\x{1F466}\x{1F467}\x{1F9D1}\x{1F468}\x{1F469}\x{1F9D3}\x{1F474}\x{1F475}\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F934}\x{1F478}\x{1F473}\x{1F472}\x{1F9D5}\x{1F9D4}\x{1F471}\x{1F935}\x{1F470}\x{1F930}\x{1F931}\x{1F47C}\x{1F385}\x{1F936}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F483}\x{1F57A}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F6C0}\x{1F6CC}\x{1F574}\x{1F3C7}\x{1F3C2}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}\x{1F933}\x{1F4AA}\x{1F9B5}\x{1F9B6}\x{1F448}\x{1F449}\x{261D}\x{1F446}\x{1F595}\x{1F447}\x{270C}\x{1F91E}\x{1F596}\x{1F918}\x{1F919}\x{1F590}\x{270B}\x{1F44C}\x{1F44D}\x{1F44E}\x{270A}\x{1F44A}\x{1F91B}\x{1F91C}\x{1F91A}\x{1F44B}\x{1F91F}\x{270D}\x{1F44F}\x{1F450}\x{1F64C}\x{1F932}\x{1F64F}\x{1F485}\x{1F442}\x{1F443}](?:\x{1F3FC})|[\x{1F476}\x{1F9D2}\x{1F466}\x{1F467}\x{1F9D1}\x{1F468}\x{1F469}\x{1F9D3}\x{1F474}\x{1F475}\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F934}\x{1F478}\x{1F473}\x{1F472}\x{1F9D5}\x{1F9D4}\x{1F471}\x{1F935}\x{1F470}\x{1F930}\x{1F931}\x{1F47C}\x{1F385}\x{1F936}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F483}\x{1F57A}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F6C0}\x{1F6CC}\x{1F574}\x{1F3C7}\x{1F3C2}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}\x{1F933}\x{1F4AA}\x{1F9B5}\x{1F9B6}\x{1F448}\x{1F449}\x{261D}\x{1F446}\x{1F595}\x{1F447}\x{270C}\x{1F91E}\x{1F596}\x{1F918}\x{1F919}\x{1F590}\x{270B}\x{1F44C}\x{1F44D}\x{1F44E}\x{270A}\x{1F44A}\x{1F91B}\x{1F91C}\x{1F91A}\x{1F44B}\x{1F91F}\x{270D}\x{1F44F}\x{1F450}\x{1F64C}\x{1F932}\x{1F64F}\x{1F485}\x{1F442}\x{1F443}](?:\x{1F3FB})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1E9}\x{1F1F0}\x{1F1F2}\x{1F1F3}\x{1F1F8}\x{1F1F9}\x{1F1FA}](?:\x{1F1FF})|[\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1F0}\x{1F1F1}\x{1F1F2}\x{1F1F5}\x{1F1F8}\x{1F1FA}](?:\x{1F1FE})|[\x{1F1E6}\x{1F1E8}\x{1F1F2}\x{1F1F8}](?:\x{1F1FD})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1F0}\x{1F1F2}\x{1F1F5}\x{1F1F7}\x{1F1F9}\x{1F1FF}](?:\x{1F1FC})|[\x{1F1E7}\x{1F1E8}\x{1F1F1}\x{1F1F2}\x{1F1F8}\x{1F1F9}](?:\x{1F1FB})|[\x{1F1E6}\x{1F1E8}\x{1F1EA}\x{1F1EC}\x{1F1ED}\x{1F1F1}\x{1F1F2}\x{1F1F3}\x{1F1F7}\x{1F1FB}](?:\x{1F1FA})|[\x{1F1E6}\x{1F1E7}\x{1F1EA}\x{1F1EC}\x{1F1ED}\x{1F1EE}\x{1F1F1}\x{1F1F2}\x{1F1F5}\x{1F1F8}\x{1F1F9}\x{1F1FE}](?:\x{1F1F9})|[\x{1F1E6}\x{1F1E7}\x{1F1EA}\x{1F1EC}\x{1F1EE}\x{1F1F1}\x{1F1F2}\x{1F1F5}\x{1F1F7}\x{1F1F8}\x{1F1FA}\x{1F1FC}](?:\x{1F1F8})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EA}\x{1F1EB}\x{1F1EC}\x{1F1ED}\x{1F1EE}\x{1F1F0}\x{1F1F1}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F8}\x{1F1F9}](?:\x{1F1F7})|[\x{1F1E6}\x{1F1E7}\x{1F1EC}\x{1F1EE}\x{1F1F2}](?:\x{1F1F6})|[\x{1F1E8}\x{1F1EC}\x{1F1EF}\x{1F1F0}\x{1F1F2}\x{1F1F3}](?:\x{1F1F5})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1E9}\x{1F1EB}\x{1F1EE}\x{1F1EF}\x{1F1F2}\x{1F1F3}\x{1F1F7}\x{1F1F8}\x{1F1F9}](?:\x{1F1F4})|[\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1ED}\x{1F1EE}\x{1F1F0}\x{1F1F2}\x{1F1F5}\x{1F1F8}\x{1F1F9}\x{1F1FA}\x{1F1FB}](?:\x{1F1F3})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1E9}\x{1F1EB}\x{1F1EC}\x{1F1ED}\x{1F1EE}\x{1F1EF}\x{1F1F0}\x{1F1F2}\x{1F1F4}\x{1F1F5}\x{1F1F8}\x{1F1F9}\x{1F1FA}\x{1F1FF}](?:\x{1F1F2})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1EE}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F8}\x{1F1F9}](?:\x{1F1F1})|[\x{1F1E8}\x{1F1E9}\x{1F1EB}\x{1F1ED}\x{1F1F1}\x{1F1F2}\x{1F1F5}\x{1F1F8}\x{1F1F9}\x{1F1FD}](?:\x{1F1F0})|[\x{1F1E7}\x{1F1E9}\x{1F1EB}\x{1F1F8}\x{1F1F9}](?:\x{1F1EF})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EB}\x{1F1EC}\x{1F1F0}\x{1F1F1}\x{1F1F3}\x{1F1F8}\x{1F1FB}](?:\x{1F1EE})|[\x{1F1E7}\x{1F1E8}\x{1F1EA}\x{1F1EC}\x{1F1F0}\x{1F1F2}\x{1F1F5}\x{1F1F8}\x{1F1F9}](?:\x{1F1ED})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1E9}\x{1F1EA}\x{1F1EC}\x{1F1F0}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F8}\x{1F1F9}\x{1F1FA}\x{1F1FB}](?:\x{1F1EC})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F9}\x{1F1FC}](?:\x{1F1EB})|[\x{1F1E6}\x{1F1E7}\x{1F1E9}\x{1F1EA}\x{1F1EC}\x{1F1EE}\x{1F1EF}\x{1F1F0}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F7}\x{1F1F8}\x{1F1FB}\x{1F1FE}](?:\x{1F1EA})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1EE}\x{1F1F2}\x{1F1F8}\x{1F1F9}](?:\x{1F1E9})|[\x{1F1E6}\x{1F1E8}\x{1F1EA}\x{1F1EE}\x{1F1F1}\x{1F1F2}\x{1F1F3}\x{1F1F8}\x{1F1F9}\x{1F1FB}](?:\x{1F1E8})|[\x{1F1E7}\x{1F1EC}\x{1F1F1}\x{1F1F8}](?:\x{1F1E7})|[\x{1F1E7}\x{1F1E8}\x{1F1EA}\x{1F1EC}\x{1F1F1}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F6}\x{1F1F8}\x{1F1F9}\x{1F1FA}\x{1F1FB}\x{1F1FF}](?:\x{1F1E6})|[\x{00A9}\x{00AE}\x{203C}\x{2049}\x{2122}\x{2139}\x{2194}-\x{2199}\x{21A9}-\x{21AA}\x{231A}-\x{231B}\x{2328}\x{23CF}\x{23E9}-\x{23F3}\x{23F8}-\x{23FA}\x{24C2}\x{25AA}-\x{25AB}\x{25B6}\x{25C0}\x{25FB}-\x{25FE}\x{2600}-\x{2604}\x{260E}\x{2611}\x{2614}-\x{2615}\x{2618}\x{261D}\x{2620}\x{2622}-\x{2623}\x{2626}\x{262A}\x{262E}-\x{262F}\x{2638}-\x{263A}\x{2640}\x{2642}\x{2648}-\x{2653}\x{2660}\x{2663}\x{2665}-\x{2666}\x{2668}\x{267B}\x{267E}-\x{267F}\x{2692}-\x{2697}\x{2699}\x{269B}-\x{269C}\x{26A0}-\x{26A1}\x{26AA}-\x{26AB}\x{26B0}-\x{26B1}\x{26BD}-\x{26BE}\x{26C4}-\x{26C5}\x{26C8}\x{26CE}-\x{26CF}\x{26D1}\x{26D3}-\x{26D4}\x{26E9}-\x{26EA}\x{26F0}-\x{26F5}\x{26F7}-\x{26FA}\x{26FD}\x{2702}\x{2705}\x{2708}-\x{270D}\x{270F}\x{2712}\x{2714}\x{2716}\x{271D}\x{2721}\x{2728}\x{2733}-\x{2734}\x{2744}\x{2747}\x{274C}\x{274E}\x{2753}-\x{2755}\x{2757}\x{2763}-\x{2764}\x{2795}-\x{2797}\x{27A1}\x{27B0}\x{27BF}\x{2934}-\x{2935}\x{2B05}-\x{2B07}\x{2B1B}-\x{2B1C}\x{2B50}\x{2B55}\x{3030}\x{303D}\x{3297}\x{3299}\x{1F004}\x{1F0CF}\x{1F170}-\x{1F171}\x{1F17E}-\x{1F17F}\x{1F18E}\x{1F191}-\x{1F19A}\x{1F201}-\x{1F202}\x{1F21A}\x{1F22F}\x{1F232}-\x{1F23A}\x{1F250}-\x{1F251}\x{1F300}-\x{1F321}\x{1F324}-\x{1F393}\x{1F396}-\x{1F397}\x{1F399}-\x{1F39B}\x{1F39E}-\x{1F3F0}\x{1F3F3}-\x{1F3F5}\x{1F3F7}-\x{1F3FA}\x{1F400}-\x{1F4FD}\x{1F4FF}-\x{1F53D}\x{1F549}-\x{1F54E}\x{1F550}-\x{1F567}\x{1F56F}-\x{1F570}\x{1F573}-\x{1F57A}\x{1F587}\x{1F58A}-\x{1F58D}\x{1F590}\x{1F595}-\x{1F596}\x{1F5A4}-\x{1F5A5}\x{1F5A8}\x{1F5B1}-\x{1F5B2}\x{1F5BC}\x{1F5C2}-\x{1F5C4}\x{1F5D1}-\x{1F5D3}\x{1F5DC}-\x{1F5DE}\x{1F5E1}\x{1F5E3}\x{1F5E8}\x{1F5EF}\x{1F5F3}\x{1F5FA}-\x{1F64F}\x{1F680}-\x{1F6C5}\x{1F6CB}-\x{1F6D2}\x{1F6E0}-\x{1F6E5}\x{1F6E9}\x{1F6EB}-\x{1F6EC}\x{1F6F0}\x{1F6F3}-\x{1F6F9}\x{1F910}-\x{1F93A}\x{1F93C}-\x{1F93E}\x{1F940}-\x{1F945}\x{1F947}-\x{1F970}\x{1F973}-\x{1F976}\x{1F97A}\x{1F97C}-\x{1F9A2}\x{1F9B0}-\x{1F9B9}\x{1F9C0}-\x{1F9C2}\x{1F9D0}-\x{1F9FF}]/u', ' ', $text);
//        $text =  preg_replace('/([0-9|#][\x{20E3}])|[\x{00ae}|\x{00a9}|\x{203C}|\x{2047}|\x{2048}|\x{2049}|\x{3030}|\x{303D}|\x{2139}|\x{2122}|\x{3297}|\x{3299}][\x{FE00}-\x{FEFF}]?|[\x{2190}-\x{21FF}][\x{FE00}-\x{FEFF}]?|[\x{2300}-\x{23FF}][\x{FE00}-\x{FEFF}]?|[\x{2460}-\x{24FF}][\x{FE00}-\x{FEFF}]?|[\x{25A0}-\x{25FF}][\x{FE00}-\x{FEFF}]?|[\x{2600}-\x{27BF}][\x{FE00}-\x{FEFF}]?|[\x{2900}-\x{297F}][\x{FE00}-\x{FEFF}]?|[\x{2B00}-\x{2BF0}][\x{FE00}-\x{FEFF}]?|[\x{1F000}-\x{1F6FF}][\x{FE00}-\x{FEFF}]?/u', ' ', $text);
//        $text = preg_replace("/\W/u"," ",$text);

        $text = preg_replace("/[^,\p{Cyrillic}\p{Latin}]/ui", ' ', $text);
        //        var_dump($text);
//        echo "<br/>";
//        $text = preg_replace("/[^а-яёa-z,]/iu", ' ', $text);;
//        var_dump(mb_substr($text,0,100,'UTF-8'));
//        echo "<br/>";
        return $text;
    }
    public  function actionTest11(){
        $text = "welcome to сервис по обмену фиатных денег в цифровую валюту btc мы работаем честно и оперативно обрабатываем и принимаем заливы от всех популярных банковских систем сбербанк тинькофф втб открытие альфа банк в наличии постоянно много материала для работы всегда выгодный курс на buy sell btc такого курса как у ℂ𝕣𝕪𝕡𝕥𝕠 𝕊𝕥𝕒𝕗𝕗 вы не найдете нигде представлен на борде dublikat с внесёнными депозитом и так же на p2p площадке bitzlato с успешно проведенными сделками контакты для работы crypto staff";
        var_dump($text);
        echo "<br/>";
        $text = $this->remove_emoji($text);
        var_dump($text);
        echo "<br/>";
        $text = preg_replace("/\W/u"," ",$text);
        var_dump($text);
        echo "<br/>";
        $text = preg_replace("/[^,\p{Cyrillic}\p{Latin}]/ui", ' ', $text);
        var_dump($text);
        echo "<br/>";
        return true;
    }

    /**
     * @param $action
     * @param string $chanel
     * @return bool|mixed|string
     */
    public function getInfo($action, $url){
        $accounts = Account::find()->where(['send_true' => false])->orderBy(['check' => SORT_DESC])->all();
//        $result = false;
        if (!$accounts) {
            $this->log($action,'error', 'нету акк');
            var_dump('нету акк');
            return false;
        }
        /** @var Account $account */
        foreach ($accounts as $account) {

            $account->datetime_last_check = date('Y-m-d H:i:s');
            try {
                $url = trim($url);
                $response = file_get_contents($account->url_server.$action.'?session='.$account->file_name.'&peer=' . $url);
            } catch (\ErrorException $e) {
                var_dump($e->getMessage());
                $this->log($action,'error1', $e->getMessage(), $url, $account->id);
                $account->check = false;
                $account->count_fuck = $account->count_fuck + 1;
                $account->save(false);
                $result = false;
                var_dump($url);
                var_dump('error');
                continue;
            }

            $this->log($action,'good1', mb_substr($this->remove_emoji($response),0,100,'UTF-8'), $url, $account->id);
            if (strpos($response, "}L")) {
                $str2=strpos($response, "}L");
                $result=substr($response, 0, $str2+1);
            } else {
                $result= $response;
            }
//            var_dump($response);
            if ($action == 'info.php') {
                if (strpos($response, "username") == false) {
                    continue;
                }
            }
            try {
                $result = unserialize(trim($result));
            } catch (\ErrorException $e) {
                var_dump($e->getMessage());
                $this->log($action,'error2', $e->getMessage(), $url, $account->id);
                $account->check = false;
                $account->count_fuck = $account->count_fuck + 1;
                $account->save(false);
                $result = false;
                var_dump('error2');
                continue;
            }
//            var_dump($result);
            $account->check = true;
            $account->count_good = $account->count_good + 1;
            $account->save(false);

            $this->log($action,'good2', null,  $url, $account->id);
            break;
        }

        return $result;
    }

    /**
     * @param $type
     * @param $text
     * @param TelegramChanel $chanel
     */
    public static function log($type, $text, $info = null, $chanel = null, $akk = null){
        if ($info) {
            $info = $info;
        } else {
            $info = '';
        }
        (new Log([
            'type' => $type,
            'text' => $text,
            'comment' => $info,
            'chanel_id' => $chanel,
            'account_id' => $akk,
            'datetime' => date('Y-m-d H:i'),
        ]))->save(false);
    }

    // /**
    //  * @param $type
    //  * @param $text
    //  * @param TelegramChanel $chanel
    //  */
    // public  function actionGetJoin(){
    //     $accaunts = Account::find()->where(['send_true' => true, 'status' => 'join', 'send_status' => 1])->limit(1)->all();
    //     /** @var Account $accaunt */
    //     foreach ($accaunts as $accaunt) {
    //         // var_dump($accaunt);

    //         $time = date('Y-m-d H:i', strtotime('-'.$accaunt->interval.' minutes'));
    //         if ($accaunt->send_time > $time and $accaunt->send_time) {
    //             continue;
    //         }

    //         $accaunt->count_send = $accaunt->count_send + $accaunt->count_send_time;
    //         $accaunt->count_good = $accaunt->count_good + $accaunt->count_send_time;

    //         if ($accaunt->count_send >= $accaunt->limit_24) {
    //             $accaunt->send_status = 0;
    //         } else {
    //             $accaunt->send_status = 1;
    //         }

    //         $accaunt->send_time = date('Y-m-d H:i:s');
    //         $accaunt->save(false);

    //         $chanels = TelegramChanel::find()
    //             ->where(['is not','username',null])
    //             ->andWhere(['can_view_participants' => 2])
    //             ->all();
    //         $ac = AccountChanel::findAll($accaunt->id);
    //         if (count($chanels) <= count($ac)) {
    //             $accaunt->status = 'send';
    //             $accaunt->save(false);
    //             continue;
    //         }

    //         $list['user'] = [
    //             'id' => $accaunt->id,
    //             'file_name' => $accaunt->file_name,
    //             'url_server' => $accaunt->url_server
    //         ];
    //         $ib = 0;
    //         /** @var TelegramChanel $chanel */
    //         foreach ($chanels as $chanel) {
    //             if ($ib >= $accaunt->count_send_time) {
    //                 break;
    //             }
    //             $ib++;

    //             $list['list'][] = [
    //                 'id' => $chanel->id,
    //                 'username' => $chanel->username
    //             ];
    //             $chanel->send_bl = false;
    //             $chanel->join_id = $accaunt->id;
    //             (new AccountChanel([
    //                 'chanel_id' => $chanel->id,
    //                 'account_id' => $accaunt->id,
    //                 'send_bl' => false,
    //                 'count_send' => 0,
    //                 'ban' => false,
    //                 'status' => false,
    //             ]))->save(false);
    //             $chanel->save(false);
    //         }
    //         return json_encode($list);
    //     }
    // }

    /**
     * @param $type
     * @param $text
     * @param TelegramChanel $chanel
     */
    public  function actionBack(){
        $chanels = TelegramChanel::find()
            ->andWhere(['is not','join_id', null])
            ->andWhere(['can_view_participants' => 2])
            ->all();

        /** @var TelegramChanel $chanel */
        foreach ($chanels as $chanel) {
            (new AccountChanel([
                'chanel_id' => $chanel->id,
                'account_id' => $chanel->join_id ,
                'send_bl' => false,
                'count_send' => 0,
                'ban' => false,
                'status' => false,
            ]))->save(false);
            $chanel->save(false);
        }
        return true;
    }


    /**
     * @param $type
     * @param $text
     * @param TelegramChanel $chanel
     */
    public  function actionGetSend(){
        $accaunts = Account::find()->where(['send_true' => true])->limit(1)->all();
        /** @var Account $accaunt */
        foreach ($accaunts as $accaunt) {
//            $time = date('Y-m-d H:i', strtotime('-'.$accaunt->interval.' minutes'));
//            if ($accaunt->send_time > $time ) {
//                continue;
//            }

            $accaunt->send_status = 1;
            $accaunt->send_time = date('Y-m-d H:i:s');
            $accaunt->save(false);

            $chanels = TelegramChanel::find()
                ->where(['is not','username',null])
                ->andWhere(['send_bl' => false])
                ->andWhere(['join_id' => $accaunt->id])
                ->andWhere(['can_view_participants' => 2])
                ->limit($accaunt->count_send_time)
                ->all();
            $list['user'] = [
                'id' => $accaunt->id,
                'file_name' => $accaunt->file_name,
                'url_server' => $accaunt->url_server,
                'message' => 'Всем привет! Кто можнет помочь с сайтом?'
            ];
            /** @var TelegramChanel $chanel */
            foreach ($chanels as $chanel) {
                $list['list'][] = [
                    'id' => $chanel->id,
                    'username' => $chanel->username
                ];
                $chanel->send_bl = true;
                $chanel->save(false);
            }
            return json_encode($list);
        }
    }

}