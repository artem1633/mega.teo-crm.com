<?php

namespace app\widgets;

use app\models\Company;
use johnitvn\ajaxcrud\CrudAsset;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;

class CompanyUsers extends Widget
{
    /** @var Company */
    public $company;

    /** @var array */
    public $containerOptions = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        if($this->company == null){
            throw new InvalidConfigException('companyId must be required');
        }

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();

        $users = $this->company->users;

        $html = '';

        foreach ($users as $user){
            if($user->is_company_super_admin == 1){
                $html .= "<p><i class='fa fa-star text-warning'></i> ".Html::a($user->name, ['user/view', 'id' => $user->id], ['role' => 'modal-remote'])."</p>";
            } else {
                $html .= "<p><i class='fa fa-user'></i> ".Html::a($user->name, ['user/view', 'id' => $user->id], ['role' => 'modal-remote'])."</p>";
            }
        }

        CrudAsset::register($this->view);

        return Html::tag('div', $html, $this->containerOptions);
    }
}