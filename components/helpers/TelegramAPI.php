<?php

namespace app\components\helpers;

use app\models\AdvertisingReport;
use app\models\TelegramChanel;

/**
 * Class TelegramAPI
 * @package app\components\helpers
 */
class TelegramAPI
{
    /**
     * @param array $condition
     */
    public static function chanelReport($condition = null)
    {
        if($condition){
            $chanels = TelegramChanel::find()->andFilterWhere($condition)->all();
        } else {
            $chanels = TelegramChanel::find()->all();
        }

        foreach ($chanels as $chanel)
        {
            $response = file_get_contents('http://tghelp2.teo-crm.com/get-subscribers.php?peer='.$chanel->url);

            $obj = unserialize($response);

            if(isset($obj['full_chat'])){
                $subsCount = $obj['full_chat']['participants_count'];
                $lastReport = AdvertisingReport::find()->where(['chanel_id' => $chanel->id])->orderBy('id desc')->one();
                if($lastReport != null){
                    $growth = $subsCount - $lastReport->subscribers_count;
                } else {
                    $growth = null;
                }
                $report = new AdvertisingReport([
                    'chanel_id' => $chanel->id,
                    'growth' => $growth,
                    'subscribers_count' => $subsCount,
                ]);
                $report->save(false);
            }
        }
    }
}