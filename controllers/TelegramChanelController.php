<?php

namespace app\controllers;

use function Amp\Dns\query;
use app\components\helpers\TelegramAPI;
use app\models\AdvertisingReportSearch;
use app\models\forms\TelegramChanelImportForm;
use app\models\KeySearch;
use app\models\MessageSearch;
use app\models\RequestSearch;
use app\models\ReviewSearch;
use app\models\TelegramChanelCompany;
use app\models\TelegramChanelKey;
use app\models\TelegramHistory;
use app\models\TelegramHistorySearch;
use app\models\TelegramPostSearch;
use app\models\TelegramUser;
use app\models\TelegramUserSearch;
use Yii;
use app\models\TelegramChanel;
use app\models\TelegramChanelSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * TelegramChanelController implements the CRUD actions for TelegramChanel model.
 */
class TelegramChanelController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TelegramChanel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TelegramChanelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if (isset($_GET['ban']) == 'true') {
            if ($_GET['ban'] == 'true') {
                $dataProvider->query->andWhere(['ban' => true]);
            }
        } else {
            $dataProvider->query->andWhere(['ban' => false]);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'isCommon' => false,
        ]);
    }

    /**
     * @param integer $id
     * @param string $pjaxContainer
     * @return mixed
     */
    public function actionUpdateChanelReport($id, $pjaxContainer = '#crud-datatable-pjax')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        TelegramAPI::chanelReport(['id' => $id]);

        return [
            'forceReload' => $pjaxContainer,
        ];
    }
    /**
     * @param integer $id
     * @param string $pjaxContainer
     * @return mixed
     */
    public function actionDelMin( $pjaxContainer = '#crud-datatable-pjax')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        TelegramChanel::deleteAll(['ban' => true, 'ban_info' => 'chanel min 500']);
        TelegramChanel::deleteAll(['ban' => true, 'ban_info' => 'chanel - type (bot or user)']);

        return [
            'forceReload' => $pjaxContainer,
        ];
    }
    /**
     * @param integer $id
     * @param string $pjaxContainer
     * @return mixed
     */
    public function actionDelBan( $pjaxContainer = '#crud-datatable-pjax')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        TelegramChanel::deleteAll(['ban' => true]);

        return [
            'forceReload' => $pjaxContainer,
        ];
    }
    /**
     * @param integer $id
     * @param string $pjaxContainer
     * @return mixed
     */
    public function actionNoBan( $pjaxContainer = '#crud-datatable-pjax')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        TelegramChanel::updateAll(['ban' => false, 'ban_info' => '', 'join_id' => null],['ban' => true]);

        return [
            'forceReload' => $pjaxContainer,
        ];
    }


    /**
     * Displays a single TelegramChanel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $companyId = null;
        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $companyId = Yii::$app->user->identity->company_id;
        }


        $messageSearchModel = new MessageSearch();
        $messageDataProvider = $messageSearchModel->search(Yii::$app->request->queryParams);
        $messageDataProvider->query->andWhere(['chanel_id' => $id]);

        $hisSearchModel = new TelegramHistorySearch();
        $hisDataProvider = $hisSearchModel->search(Yii::$app->request->queryParams);
        $hisDataProvider->query->andWhere(['chanel_id' => $id]);



        $subscribersReport = [
            'dates' => [],
            'subscribers' => [],
            'posts' => [],
        ];

//            $date = date("Y-m-d", strtotime($date));
        $historys = TelegramHistory::find()
            ->where(['chanel_id' => $id])
            ->andWhere(['like', 'created_at', date('Y-m-d')])
            ->all();

        if($historys){
            foreach ($historys as $history) {
                $subscribersReport['date'][] = $history->created_at;
                $subscribersReport['subscribers'][] = $history->subscribers_count;
                $subscribersReport['posts'][] = $history->posts_count;
            }
        }

//        $subscribersReport = [
//            'dates' => $report[0],
//            'subscribers' => ArrayHelper::getColumn($history, 'subscribers_count'),
//            'posts' => ArrayHelper::getColumn($history, 'posts_count'),
//        ];

        $isAssigned = true;


        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "канал #".$id,
                'content'=>$this->renderAjax('view', [
                    'model' => $model,
                    'messageSearchModel' => $messageSearchModel,
                    'messageDataProvider' => $messageDataProvider,
                    'hisSearchModel' => $hisSearchModel,
                    'hisDataProvider' => $hisDataProvider,
                    'subscribersReport' => $subscribersReport,
                    'isAssigned' => $isAssigned,

                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view', [
                'model' => $model,
                'messageSearchModel' => $messageSearchModel,
                'messageDataProvider' => $messageDataProvider,
                'hisSearchModel' => $hisSearchModel,
                'hisDataProvider' => $hisDataProvider,
                'subscribersReport' => $subscribersReport,
                'isAssigned' => $isAssigned,
            ]);
        }
    }

    /**
     * Creates a new TelegramChanel model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new TelegramChanel();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить канал",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Добавить канал",
                    'content'=>'<span class="text-success">Создание канала успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose' => true,
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing TelegramChanel model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить канал #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose' => true,
                ];
            }else{
                return [
                    'title'=> "Изменить канал #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * @return array
     */
    public function actionImport()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new TelegramChanelImportForm();

        if($model->load($request->post()) && $model->import()){
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Импортировать каналы",
                'content'=>$this->renderAjax('import', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Импорт',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function actionAssignCompany($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;


        return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function actionDeleteRelation($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $relation = TelegramChanelCompany::find()->where(['company_id' => Yii::$app->user->identity->company_id, 'telegram_chanel_id' => $id])->one();

        if($relation){
            $relation->delete();
        }

        return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
    }

    /**
     * Delete an existing TelegramChanel model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing TelegramChanel model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the TelegramChanel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TelegramChanel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TelegramChanel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
