<?php

use yii\db\Migration;

/**
 * Class m200229_185352_addd_colums_config
 */
class m200229_185352_addd_colums_config extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings',array(
            'key' => 'time_post',
            'value' => '600',
            'label' => 'Время обновления постов',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => 'time_post']);
    }
}
