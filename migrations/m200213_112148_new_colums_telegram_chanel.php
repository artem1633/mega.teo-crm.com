<?php

use yii\db\Migration;

/**
 * Class m200213_112148_new_colums_telegram_chanel
 */
class m200213_112148_new_colums_telegram_chanel extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('telegram_chanel', 'join', $this->boolean()->comment('Вступили'));
        $this->addColumn('telegram_chanel', 'join_id', $this->integer()->comment('Кто вступил'));
        $this->addColumn('telegram_chanel', 'send_true', $this->boolean()->comment('Разрешена отправка'));
        $this->addColumn('telegram_chanel', 'send_bl', $this->boolean()->comment('Отправка произведена'));
        $this->addColumn('telegram_chanel', 'send_time', $this->dateTime()->comment('Дата и время отправки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('telegram_chanel', 'join');
        $this->dropColumn('telegram_chanel', 'join_id');
        $this->dropColumn('telegram_chanel', 'send_true');
        $this->dropColumn('telegram_chanel', 'send_bl');
        $this->dropColumn('telegram_chanel', 'send_time');
    }
}
