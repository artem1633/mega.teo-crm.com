<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%log}}`.
 */
class m200205_170504_create_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%log}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string()->comment('Тип'),
            'text' => $this->text()->comment('Текст'),
            'comment' => $this->text()->comment('Комментарий'),
            'datetime' => $this->dateTime()->comment('Дата и время'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%log}}');
    }
}
