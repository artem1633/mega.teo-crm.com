<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%telegram_history}}`.
 */
class m200202_141516_create_telegram_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%telegram_history}}', [
            'id' => $this->primaryKey(),
            'chanel_id' => $this->integer()->comment('Канал'),
            'subscribers_count' => $this->integer()->comment('Кол-во подписчиков'),
            'posts_count' => $this->integer()->comment('Кол-во постов'),
            'last_post_id' => $this->string()->comment('ID последнего поста'),
            'company_id' => $this->integer()->comment('Компания'),
            'created_at' => $this->date(),
        ]);

        $this->createIndex(
            'idx-telegram_history-chanel_id',
            'telegram_history',
            'chanel_id'
        );

        $this->addForeignKey(
            'fk-telegram_history-chanel_id',
            'telegram_history',
            'chanel_id',
            'telegram_chanel',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-telegram_history-company_id',
            'telegram_history',
            'company_id'
        );

        $this->addForeignKey(
            'fk-telegram_history-company_id',
            'telegram_history',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-telegram_history-company_id',
            'telegram_history'
        );

        $this->dropIndex(
            'idx-telegram_history-company_id',
            'telegram_history'
        );

        $this->dropForeignKey(
            'fk-telegram_history-chanel_id',
            'telegram_history'
        );

        $this->dropIndex(
            'idx-telegram_history-chanel_id',
            'telegram_history'
        );

        $this->dropTable('{{%telegram_history}}');
    }
}
