<?php

use yii\db\Migration;

/**
 * Class m200229_152040_addd_colums_history
 */
class m200229_152040_addd_colums_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('telegram_history', 'grown', $this->integer()->comment('прирост'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('telegram_history', 'grown');
    }
}
