<?php

use yii\db\Migration;

/**
 * Class m200229_195236_add_colums_history_grown
 */
class m200229_195236_add_colums_history_grown extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('telegram_history', 'grown', $this->integer()->comment('прирост'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('telegram_history', 'grown');
    }
}
