<?php

use yii\db\Migration;

/**
 * Class m200208_061318_add_to_log
 */
class m200208_061318_add_to_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('log', 'chanel_id', $this->integer()->comment('Канал'));
        $this->addColumn('log', 'account_id', $this->integer()->comment('Акк'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('log', 'chanel_id');
        $this->dropColumn('log', 'account_id');
    }
}
