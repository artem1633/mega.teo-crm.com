<?php

use yii\db\Migration;

/**
 * Class m200207_140638_account_table
 */
class m200207_140638_account_table extends Migration
{/**
 * @inheritdoc
 */
    public function up()
    {
        $this->createTable('account', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'username' => $this->string()->comment('Ссылка'),
            'count_good' => $this->integer()->comment('Кол-во запросов удачных'),
            'count_fuck' => $this->integer()->comment('Кол-во запросов не удачных'),
            'numbur' => $this->string()->comment('Номер'),
            'api' => $this->string()->comment('API CODE'),
            'hash' => $this->string()->comment('HASH'),
            'file_name' => $this->string()->comment('Название файла'),
            'check' => $this->boolean()->comment('Актиный'),
            'url_server' => $this->string()->comment('URL сервера'),
            'code_file' => $this->text()->comment('Код файла'),
            'datetime_last_compare' => $this->dateTime()->comment('Дата и время последнего совпадения'),
            'datetime_last_check' => $this->dateTime()->comment('Дата и время последней проверки'),
            'created_at' => $this->dateTime(),
        ]);

        $this->addColumn('account', 'count_send_time', $this->integer()->comment('Кол-во каналов за раз'));
        $this->addColumn('account', 'interval', $this->integer()->comment('Интервал отправки'));
        $this->addColumn('account', 'send_status', $this->boolean()->comment('Статус отправки'));
        $this->addColumn('account', 'send_time', $this->dateTime()->comment('Время отправки последнего'));
        $this->addColumn('account', 'limit_24', $this->integer()->comment('Лимит в сутки'));
        $this->addColumn('account', 'status', $this->string()->comment('Статус'));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('account');
    }
}
