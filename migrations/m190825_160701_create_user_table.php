<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m190825_160701_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull()->comment('Email'),
            'name' => $this->string()->comment('ФИО'),
            'company_id' => $this->integer()->comment('Компания'),
            'phone' => $this->string()->comment('Телефон'),
            'is_company_super_admin' => $this->boolean()->comment('Является ли администратором компании'),
            'access' => $this->boolean()->defaultValue(true)->comment('Доступ (вкл/выкл)'),
            'role' => $this->integer()->comment('Роль'),
            'password_hash' => $this->string()->notNull()->comment('Зашифрованный пароль'),
            'last_activity_datetime' => $this->dateTime()->comment('Дата и время последней активности'),
            'is_deletable' => $this->boolean()->notNull()->defaultValue(true)->comment('Можно удалить или нельзя'),
            'created_at' => $this->dateTime(),
        ]);

        $this->insert('user', [
            'email' => 'admin@admin.com',
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'is_deletable' => false,
            'is_company_super_admin' => true,
            'company_id' => 1,
        ]);

        $this->createIndex(
            'idx-user-company_id',
            'user',
            'company_id'
        );

        $this->addForeignKey(
            'fk-user-company_id',
            'user',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-user-company_id',
            'user'
        );

        $this->dropIndex(
            'idx-user-company_id',
            'user'
        );

        $this->dropTable('user');
    }
}
