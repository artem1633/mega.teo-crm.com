<?php

use yii\db\Migration;

/**
 * Class m200229_130836_addd_colums_settings
 */
class m200229_130836_addd_colums_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings',array(
            'key' => 'chanel_count_account',
            'value' => '100',
            'label' => 'Кол-во каналов на акк',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => 'chanel_count_account']);
    }

}
