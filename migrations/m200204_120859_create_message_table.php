<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%message}}`.
 */
class m200204_120859_create_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%message}}', [
            'id' => $this->primaryKey(),
            'chanel_id' => $this->integer()->comment('Канал'),
            'text' => $this->text()->comment('Текст'),
            'text_id' => $this->string()->comment('ID текста'),
            'datetime' => $this->dateTime()->comment('Дата и время'),
            'company_id' => $this->integer()->comment('Компания'),
        ]);

        $this->createIndex(
            'idx-message-chanel_id',
            'message',
            'chanel_id'
        );

        $this->addForeignKey(
            'fk-message-chanel_id',
            'message',
            'chanel_id',
            'telegram_chanel',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-message-company_id',
            'message',
            'company_id'
        );

        $this->addForeignKey(
            'fk-message-company_id',
            'message',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-message-company_id',
            'message'
        );

        $this->dropIndex(
            'idx-message-company_id',
            'message'
        );

        $this->dropForeignKey(
            'fk-message-chanel_id',
            'message'
        );

        $this->dropIndex(
            'idx-message-chanel_id',
            'message'
        );

        $this->dropTable('{{%message}}');
    }
}
