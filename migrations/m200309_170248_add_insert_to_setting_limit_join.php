<?php

use yii\db\Migration;

/**
 * Class m200309_170248_add_insert_to_setting_limit_join
 */
class m200309_170248_add_insert_to_setting_limit_join extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings',array(
            'key' => 'limit_join',
            'value' => '200',
            'label' => 'Лимит вступлеений в сутки',
        ));
        $this->insert('settings',array(
            'key' => 'limit_acc_time',
            'value' => '10',
            'label' => 'Лимит акк за один запрос',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => 'limit_join']);
        \app\models\Settings::deleteAll(['key' => 'limit_acc_time']);
    }
}
