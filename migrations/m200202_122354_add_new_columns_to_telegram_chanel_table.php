<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%telegram_chanel}}`.
 */
class m200202_122354_add_new_columns_to_telegram_chanel_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('telegram_chanel', 'img', $this->string()->comment('Превью канала'));
        $this->addColumn('telegram_chanel', 'external_id', $this->string()->comment('Внещний ID канала'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('telegram_chanel', 'img');
        $this->dropColumn('telegram_chanel', 'external_id');
    }
}
