<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%telegram_chanel}}`.
 */
class m200203_122751_add_new_columns_to_telegram_chanel_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('telegram_chanel', 'username', $this->string()->comment('Имя пользователя'));
        $this->addColumn('telegram_chanel', 'can_view_participants', $this->boolean()->comment('Можно ли смотреть подписчиков'));
        $this->addColumn('telegram_chanel', 'type', $this->string()->comment('Тип'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('telegram_chanel', 'username');
        $this->dropColumn('telegram_chanel', 'can_view_participants');
        $this->dropColumn('telegram_chanel', 'type');
    }
}
