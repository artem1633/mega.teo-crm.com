<?php

use yii\db\Migration;

/**
 * Class m200309_165841_add_to_chanel_ban_ban_info_ban_date_join_date
 */
class m200309_165841_add_to_chanel_ban_ban_info_ban_date_join_date extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('telegram_chanel', 'ban', $this->boolean()->defaultValue(false)->comment('Ban'));
        $this->addColumn('telegram_chanel', 'ban_info', $this->text()->comment('Инфа о бане'));
        $this->addColumn('telegram_chanel', 'ban_date', $this->dateTime()->comment('Дата и время бана'));
        $this->addColumn('telegram_chanel', 'join_date', $this->dateTime()->comment('Дата и время вступления'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('telegram_chanel', 'ban');
        $this->dropColumn('telegram_chanel', 'ban_info');
        $this->dropColumn('telegram_chanel', 'ban_date');
        $this->dropColumn('telegram_chanel', 'join_date');
    }
}
