<?php

use yii\db\Migration;

/**
 * Class m200204_130932_insert_username_admin_setting
 */
class m200204_130932_insert_username_admin_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'username_admin',
            'label' => 'Логин телеграмма админа'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => 'username_admin']);
    }
}
