<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company`.
 */
class m190825_160659_create_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('ФИО'),
            'phone' => $this->string()->comment('Телефон'),
            'refer_id' => $this->integer()->comment('Кто пригласил'),
            'is_super_company' => $this->boolean()->defaultValue(false)->comment('Является ли супер компанией'),
            'access' => $this->boolean()->defaultValue(true)->comment('Доступ (вкл/выкл)'),
            'last_activity_datetime' => $this->dateTime()->comment('Дата и время последней активности'),
            'balance' => $this->float()->comment('Баланс'),
            'partner_balance' => $this->float()->comment('Партнерский баланс'),
            'promo_code_id' => $this->integer()->comment('Промо код'),
            'created_at' => $this->dateTime(),
        ]);

        $this->insert('company', [
            'name' => 'Супер компания',
            'is_super_company' => 1
        ]);

        $this->createIndex(
            'idx-company-refer_id',
            'company',
            'refer_id'
        );

        $this->addForeignKey(
            'fk-company-refer_id',
            'company',
            'refer_id',
            'company',
            'id',
            'SET NULL'
        );


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-company-refer_id',
            'company'
        );

        $this->dropIndex(
            'idx-company-refer_id',
            'company'
        );


        $this->dropTable('company');
    }
}
