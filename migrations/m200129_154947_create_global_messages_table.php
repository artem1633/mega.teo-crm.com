<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%global_messages}}`.
 */
class m200129_154947_create_global_messages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('global_messages', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'text' => $this->binary()->comment('Текст'),
            'views_count' => $this->integer()->unsigned()->defaultValue(0)->comment('Кол-во просмотров'),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('global_messages');
    }
}
