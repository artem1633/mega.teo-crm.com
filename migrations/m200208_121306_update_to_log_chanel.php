<?php

use yii\db\Migration;

/**
 * Class m200208_121306_update_to_log_chanel
 */
class m200208_121306_update_to_log_chanel extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->dropColumn('log', 'chanel_id');
        $this->addColumn('log', 'chanel_id', $this->string()->comment('Канал'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropColumn('log', 'chanel_id');
        $this->addColumn('log', 'chanel_id', $this->integer()->comment('Канал'));
    }
}
