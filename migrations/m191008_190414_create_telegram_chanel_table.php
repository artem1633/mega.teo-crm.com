<?php

use yii\db\Migration;

/**
 * Handles the creation of table `telegram_chanel`.
 */
class m191008_190414_create_telegram_chanel_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('telegram_chanel', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->comment('Компания'),
            'name' => $this->string()->comment('Наименование'),
            'description' => $this->text()->comment('Описание'),
            'url' => $this->string()->comment('Ссылка'),
            'subscribers_count' => $this->integer()->comment('Кол-во подписчиков'),
            'count_posts_today' => $this->integer()->comment('Кол-во постов за сегодня'),
            'advertising_price' => $this->float()->comment('Стоимость рекламы'),
            'last_post_id' => $this->integer()->comment('ID последнего поста'),
            'last_post_text' => $this->binary()->comment('Текст поста'),
            'last_post_author' => $this->string()->comment('Автор'),
            'growth' => $this->integer()->comment('Прирост'),
            'datetime_last_compare' => $this->dateTime()->comment('Дата и время последнего совпадения'),
            'datetime_last_check' => $this->dateTime()->comment('Дата и время последней проверки'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-telegram_chanel-company_id',
            'telegram_chanel',
            'company_id'
        );

        $this->addForeignKey(
            'fk-telegram_chanel-company_id',
            'telegram_chanel',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-telegram_chanel-company_id',
            'telegram_chanel'
        );

        $this->dropIndex(
            'idx-telegram_chanel-company_id',
            'telegram_chanel'
        );

        $this->dropTable('telegram_chanel');
    }
}
