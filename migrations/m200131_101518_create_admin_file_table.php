<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%admin_file}}`.
 */
class m200131_101518_create_admin_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%admin_file}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'path' => $this->string()->comment('Путь'),
            'upload_at' => $this->dateTime()->comment('Дата и время загрузки'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%admin_file}}');
    }
}
