<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TelegramHistory */

?>
<div class="telegram-history-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
