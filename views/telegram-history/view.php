<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TelegramHistory */
?>
<div class="telegram-history-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'chanel_id',
            'subscribers_count',
            'posts_count',
            'last_post_id',
            'company_id',
            'created_at',
        ],
    ]) ?>

</div>
