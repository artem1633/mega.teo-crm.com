<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TelegramHistory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="telegram-history-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'chanel_id')->textInput() ?>

    <?= $form->field($model, 'subscribers_count')->textInput() ?>

    <?= $form->field($model, 'posts_count')->textInput() ?>

    <?= $form->field($model, 'last_post_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'company_id')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
