<?php

use yii\widgets\ActiveForm;
use app\models\AdminFile;

/**
 * @var $this \yii\web\View
 * @var $settings \app\models\Settings[]
 * @var $fileModel \app\models\AdminFile
 */

$this->title = 'Настройки';

?>


<div class="panel panel-inverse news-index">
    <div class="panel-heading">
        <!--        <div class="panel-heading-btn">-->
        <!--        </div>-->
        <h4 class="panel-title">Новости</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <?php if (count($settings) > 0) { ?>
                    <form method="post" class="form-horizontal form-bordered" style="padding: 0;">
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                               value="<?= Yii::$app->request->csrfToken ?>">
                        <?php foreach ($settings as $setting): ?>
                            <div class="form-group">
                                <label for=""
                                       class="control-label col-md-2"><?= Yii::$app->formatter->asRaw($setting->label) ?></label>
                                <div class="col-md-10">
                                    <div class="col-md-8">
                                            <input name="Settings[<?= $setting->key ?>]" type="text"
                                                   value="<?= $setting->value ?>" class="form-control">
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
<!--                        <div class="form-group">-->
<!--                            <label for="" class="control-label col-md-2">Файл инструкции</label>-->
<!--                            <div class="col-md-10">-->
<!--                                <div class="col-md-8">-->
                                    <?php
//                                    $form = ActiveForm::begin(['id' => 'admin-file-form', 'action' => ['upload-file']])
                                    ?>
<!--                                        <div class="hidden">-->
                                            <?php
//                                            echo $form->field($fileModel, 'file')->fileInput()
                                            ?>
<!--                                        </div>-->
<!--                                    <a id="file-upload-btn" href="#" class="btn btn-sm btn-info">Загрузите файл</a>-->
                                    <?php
//                                    ActiveForm::end()
                                    ?>
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="form-group">
                            <label for="" class="control-label col-md-2"></label>
                            <div class="col-md-10">
                                <div class="input-group">
                                    <button type="submit" style="margin-left: 15px;"
                                            class="btn btn-success btn-sm">Сохранить
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                <?php } else { ?>
                    <div class="note note-danger" style="margin: 15px;">
                        <h4><i class="fa fa-exclamation-triangle"></i> Настроек в базе данных не обнаружено!
                        </h4>
                        <p>
                            Убедитесь, что все миграции выполнены без ошибок
                        </p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<?php

$script = <<< JS

$('#file-upload-btn').click(function(e){
    e.preventDefault();
    $('#adminfile-file').trigger('click');
});

$('#adminfile-file').change(function(e){
    e.preventDefault();
    $('#admin-file-form').submit();
});

$('#admin-file-form').submit(function(e){
    e.preventDefault();
    alert(123);
     var formData = new FormData($(this)[0]);

        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: formData,
            async: false,
            success: function (data) {
                console.log(data);
            },
            cache: false,
            contentType: false,
            processData: false
        });
        
        return false;
});

JS;

//$this->registerJs($script, \yii\web\View::POS_READY);


?>