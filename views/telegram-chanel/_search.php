<?php

use kartik\form\ActiveForm;

/** @var $this \yii\web\View */
/** @var $model \app\models\TelegramChanelSearch */

?>

        <?php $form = ActiveForm::begin(['id' => 'search-form', 'method' => 'GET', 'options' => ['style' => 'width: 20%; display: inline-block;']]) ?>

          <div style="padding: 0 10px;">
              <?= $form->field($model, 'name', ['template' => '<div class="form-group" style="margin-top: 10px;">{input}</div>'])->textInput(['placeholder' => 'Поиск'])->label(false) ?>
          </div>

        <?php ActiveForm::end() ?>

<?php

$script = <<< JS

$('#telegramchanelsearch-name').change(function(){
    $('#search-form').submit();
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);
?>