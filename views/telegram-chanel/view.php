<?php

use skeeks\widget\highcharts\Highcharts;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\TelegramChanel */
/* @var $isAssigned boolean */


$arr1 = [
    2, 3
];

$arr2 = [
    2, 3
];

\johnitvn\ajaxcrud\CrudAsset::register($this);


$this->title = "Канал «{$model->name}»";





?>
    <div class="telegram-chanel-view">

        <div class="col-md-7">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">График</h4>
                </div>
                <div class="panel-body" style="height: 250px; overflow-y: auto;">

                    <?php if(isset($subscribersReport['date']) == false || isset($subscribersReport['subscribers']) == false || isset($subscribersReport['posts']) == false): ?>

                        <h4 class="text-center">Отсутствуют данные анализа</h4>

                    <?php else: ?>

                        <?= Highcharts::widget([
                            'options' => [
                                'chart' => [
                                    'height' => 225,
                                    'type' => 'line'

                                ],
                                'title' => false,//['text' => 'Всего отправлено сообщений'],
                                //'subtitle' => ['text' => 'Notice the difference between a 0 value and a null point'],
                                'plotOptions' => [
                                    'column' => ['depth' => 25],
                                    'line' => [
                                        'dataLabels' => [
                                            'enabled' => true
                                        ]
                                    ],
                                    'enableMouseTracking' => false,
                                ],
                                'xAxis' => [
                                    'categories' => $subscribersReport['date'],
                                    'labels' => [
                                        'skew3d' => true,
                                        'style' => ['fontSize' => '10px']
                                    ]
                                ],
                                'yAxis' => [
                                    'title' => ['text' => null]
                                ],
                                'series' => [
                                    ['name' => 'Подписчики', 'data' => $subscribersReport['subscribers']],
                                    ['name' => 'Посты', 'data' => $subscribersReport['posts']],
                                ],
                                'credits' => ['enabled' => false],
                                'legend' => ['enabled' => true],
                            ]
                        ]);
                        ?>

                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Информация</h4>
                </div>
                <div class="panel-body" style="height: 250px; overflow-y: auto;">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
//                                [
//                                    'attribute' => 'id',
//                                    'visible' => Yii::$app->user->identity->isSuperAdmin(),
//                                ],
//                            [
//                                'attribute' => 'company_id',
//                                'visible' => Yii::$app->user->identity->isSuperAdmin(),
//                            ],

                            'name',
                            'description:ntext',
                            'url:url',
                            'subscribers_count',
                            'count_posts_today',
                            [
                                'attribute' => 'last_post_id',
                                'visible' => Yii::$app->user->identity->isSuperAdmin(),
                            ],
                            'last_post_text',
//                                'last_post_author',
//                                'growth',
                            'datetime_last_compare',
                            'ban',
                            'ban_info',
                            'ban_date',
                            'join_date',
//                                'created_at',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>




            <div class="col-md-7">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Сообщения</h4>
                    </div>
                    <div class="panel-body" style="height: 700px; overflow-y: auto;">
                        <?= $this->render('@app/views/telegram-chanel/index-message', [
                            'searchModel' => $messageSearchModel,
                            'dataProvider' => $messageDataProvider,
                            'chanelId' => $model->id,
                        ]) ?>
                    </div>
                </div>
            </div>

        <div class="col-md-5">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">История</h4>
                </div>
                <div class="panel-body" style="height: 700px; overflow-y: auto;">
                    <?=GridView::widget([
                        'id'=>'crud-datatable',
                        'dataProvider' => $hisDataProvider,
                        'filterModel' => $hisSearchModel,
                        'pjax'=>true,
                        'columns' => [
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'subscribers_count',
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'grown',
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'posts_count',
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'last_post_id',
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'created_at',
                            ],
                            // [
                            // 'class'=>'\kartik\grid\DataColumn',
                            // 'attribute'=>'account_id',
                            // ],
                        ]
                    ])?>

                </div>
            </div>
        </div>

    </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>