<?php

use app\models\Settings;
use app\models\TelegramUser;
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Лого',
        'content' => function($model){
            return '<img src="'.$model->realImg.'" style="height: 35px; width: 35px; border-radius: 1em; border: 1px solid #fefefe; object-fit: cover;">';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Канал',
        'content' => function($model){
            $a = '';


            $count = '';
            if (!$model->username) {
                $url = $model->url;
            } else {
                $url = $model->username;
            }
            $b = Html::a('@'.$url, 'tg://resolve?domain='.$url, [
                'title'=>'Перейти', 'blank' => '_target',
            ]);
            return $model->name.' '.$a.' <i class="fa fa-arrow-up text-success">'.$model->count_posts_today.'</i><br/>'.$b.' - '.$model->subscribers_count.' чел. <span style="color:green;">'.$count.'</span><br/>'.$model->type;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Описание',
        'width' => '20%',
        'content' => function($model){
            $info = mb_substr($model->description,0,80,'UTF-8');
            if ($model->ban) {
                $info = $model->ban_info. ' - '.$model->ban_date;
            }
            return $info;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'last_post_text',
        'width' => '20%',
        'content' => function($model){
            $date_2 = date("Y-m-d H:i");
            $interval_min = Settings::findByKey('interval_chanel')->value;;
            $diff = strtotime($date_2) - strtotime($model->datetime_last_check) ;
            $diff = round($interval_min - $diff /60);

            return "<span style='color:darksalmon'> Обновиться через {$diff} минут</span><br/>".mb_substr($model->last_post_text,0,80,'UTF-8'); ;
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'subscribers_count',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'Подписчики',
//        'content' => function($model){
//            if($model->can_view_participants == 1){
//                return '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
//            } else if($model->can_view_participants == 0) {
//                return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
//            }
//        },
//        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
//        'width' => '5%',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'company_id',
//        'value' => 'company.name',
//        'visible' => Yii::$app->user->identity->isSuperAdmin(),
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'img',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'verify_mark',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'discount_mark',
//    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'section',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'growth',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'coverage',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'er',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'telegraph',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to(['telegram-chanel/'.$action,'id'=>$key]);
        },
        'template' => '{view} {update}{delete}',
        'buttons' => [
            'view' => function ($url, $model) {
                return Html::a('<i class="fa fa-eye text-info" style="font-size: 16px;"></i>', $url, [
                        'title'=>'Посмотреть', 'data-pjax' => '0',
                    ])."&nbsp;";
            },
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                        'role'=>'modal-remote', 'title'=>'Изменить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                    ])."&nbsp;";
            }
        ],
    ],

];   