<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\TelegramChanel;
use app\models\TelegramChanelKey;
use app\models\KeyOne;
use app\models\KeyOneMinus;


return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'equals_count',
    ],
    [
        'label' => 'Слова',
        'content' => function($model){
            $names = implode(', ', ArrayHelper::getColumn(KeyOne::find()->where(['key_id' => $model->id])->all(), 'value'));

            $names = mb_substr($names,0,80,'UTF-8');

            return $names;
        }
    ],
    [
        'label' => 'Слова отрицательные',
        'content' => function($model){
            $names = implode(', ', ArrayHelper::getColumn(KeyOneMinus::find()->where(['key_id' => $model->id])->all(), 'value'));

            $names = mb_substr($names,0,80,'UTF-8');
            return $names;
        }
    ],
    [
        'label' => 'Каналы',
        'content' => function($model){
            $pks = ArrayHelper::getColumn(TelegramChanelKey::find()->where(['key_id' => $model->id])->all(), 'telegram_chanel_id');
            $names = implode(', ', ArrayHelper::getColumn(TelegramChanel::findAll($pks), 'name'));

            $names = mb_substr($names,0,80,'UTF-8');
            return $names;
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'company_id',
//    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to(['key/'.$action, 'id' => $key, 'pjaxContainer' => '#crud-key-datatable-pjax']);
        },
        'template' => '{update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                        'role'=>'modal-remote', 'title'=>'Изменить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                    ])."&nbsp;";
            }
        ],
    ],

];