<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TelegramChanel */

?>
<div class="telegram-chanel-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
