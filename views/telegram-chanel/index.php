<?php

use app\models\Message;
use app\models\TelegramChanel;
use app\models\TelegramUser;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\models\Settings;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TelegramChanelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $isCommon boolean */


CrudAsset::register($this);

$form = \kartik\form\ActiveForm::begin();

$panelBeforeTemplate = '';



$time = date('Y-m-d H:i', strtotime('-100 minutes'));
$time2 = date('Y-m-d H:i', strtotime('-200 minutes'));
$timeOk = TelegramChanel::find()->where(['>','datetime_last_check', $time])->count();
$time100 = TelegramChanel::find()->where(['<','datetime_last_check', $time])->count();
$time200 = TelegramChanel::find()->where(['<','datetime_last_check', $time2])->count();
$ban = TelegramChanel::find()->where(['ban'=> true])->count();



$panelBeforeTemplate = '';
$all_dar_akk = 0;
$coud_los = 0;
$countMes = Message::find()->count();
$countMes2 = TelegramChanel::find()->where(['is','username',null])->count();
$dat = date('Y-m-d');
$mesAll = Message::find()->where(['like', 'datetime', $dat])->count();
$info = "
                     <span class='text-info'>Всего каналов: {$dataProvider->getTotalCount()}<span/> /  
                     <span class='text-danger'>В обработкее: {$countMes2}</span> /
                     <span class='text-success'> Кол-во постов всего: {$countMes}</span> /
                     <span class='text-success'> Кол-во постов сегодня: {$mesAll}</span> /
                     <span class='text-success'> Меньше 100 мин: {$timeOk}</span> /
                     <span class='text-danger'> Больше 100 мин: {$time100}</span> /
                     <span class='text-danger'> Больше 200 мин: {$time200}</span> /
                     <span class='text-danger'> Бан: {$ban}</span> ";

if($isCommon){

    $this->title = "Общие Телеграмм каналы";

    if(Yii::$app->user->identity->isSuperAdmin()){
        $panelBeforeTemplate = Html::a('Добавить <i class="fa fa-plus"></i>', ['telegram-chanel/create'],
                ['role'=>'modal-remote','title'=> 'Добавить канал','class'=>'btn btn-success']).'&nbsp;'.
            Html::a('<i class="fa fa-repeat"></i>', [''],
                ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']);
    }
} else {

    $this->title = "Мои Телеграмм каналы";

    $panelBeforeTemplate = Html::a('Добавить <i class="fa fa-plus"></i>', ['telegram-chanel/create'],
            ['role'=>'modal-remote','title'=> 'Добавить канал','class'=>'btn btn-success']).'&nbsp;'.
        Html::a('<i class="fa fa-repeat"></i>', [''],
            ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']).'&nbsp;'.Html::a('Импорт', 'telegram-chanel/import', ['role' => 'modal-remote', 'class' => 'btn btn-info']);
}

$after = '';

if(Yii::$app->user->identity->isSuperAdmin()){
    $after = BulkButtonWidget::widget([
            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                ["telegram-chanel/bulk-delete"] ,
                [
                    "class"=>"btn btn-danger btn-xs",
                    'role'=>'modal-remote-bulk',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
                ]),
        ]).
        '<div class="clearfix"></div>';
}

//$panelBeforeTemplate .= '&nbsp;'.Html::a('Импорт', 'telegram-chanel/import', ['role' => 'modal-remote', 'class' => 'btn btn-info']);

$this->params['breadcrumbs'][] = $this->title;


$panelBeforeTemplate .= $this->render('_search', [
    'model' => $searchModel,
]);

$panelBeforeTemplate .= Html::a('<i class="fa fa-close"></i> Удалить min 500', '/telegram-chanel/del-min/', [
    'role'=>'modal-remote', 'title'=>'Удалить', 'class' => 'btn btn-warning',
    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
    'data-request-method'=>'post',
    'data-confirm-title'=>'Вы уверены?',
    'data-confirm-message'=>'Вы действительно хотите удалить все записи с текущим статусом?'
]);
$panelBeforeTemplate .= Html::a('<i class="fa fa-close"></i> Снять бан', '/telegram-chanel/no-ban/', [
    'role'=>'modal-remote', 'title'=>'Удалить', 'class' => 'btn btn-warning',
    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
    'data-request-method'=>'post',
    'data-confirm-title'=>'Вы уверены?',
    'data-confirm-message'=>'Вы действительно хотите удалить все записи с текущим статусом?'
]);
$panelBeforeTemplate .= Html::a('<i class="fa fa-close"></i> Удалить все забаненые', '/telegram-chanel/del-ban/', [
    'role'=>'modal-remote', 'title'=>'Удалить', 'class' => 'btn btn-warning',
    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
    'data-request-method'=>'post',
    'data-confirm-title'=>'Вы уверены?',
    'data-confirm-message'=>'Вы действительно хотите удалить все записи с текущим статусом?'
]);

?>
<div class="panel panel-inverse telegram-chanel-index">
    <div class="panel-heading">
        <!--        <div class="panel-heading-btn">-->
        <!--        </div>-->
        <h4 class="panel-title"><?=$info ?></h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
                'pjax'=>true,
                'columns' => [
                    [
                        'class' => 'kartik\grid\CheckboxColumn',
                        'width' => '20px',
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'width' => '30px',
                    ],
                    // [
                    // 'class'=>'\kartik\grid\DataColumn',
                    // 'attribute'=>'id',
                    // ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'label' => 'Лого',
                        'width' => '5%',
                        'content' => function($model){
                            return '<img src="'.$model->realImg.'" style="height: 50px; width: 50px; border-radius: 1em; border: 1px solid #fefefe; object-fit: cover;">';
                        }
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'label'=>'Канал',
                        'width' => '15%',
                        'content' => function($model){
                            $a = '';


                            $count = '';
                            if (!$model->username) {
                                $url = $model->url;
                            } else {
                                $url = $model->username;
                            }
                            $b = Html::a('@'.$url, 'tg://resolve?domain='.$url, [
                                'title'=>'Перейти', 'blank' => '_target',
                            ]);
                            $dat = date('Y-m-d');
                            $mesAll = Message::find()->where(['like', 'datetime', $dat])->andWhere(['chanel_id' => $model->id])->count();

                            $link = Html::a(mb_substr($model->name,0,20,'UTF-8'),
                                ['/telegram-chanel/view?id='.$model->id],
                                ['title'=>'Посмотреть', 'data-pjax' => '0',]);
                            return $link.' '.$a.' <i class="fa fa-arrow-up text-success">'.$mesAll.'</i><br/>'.$b.' - '.$model->subscribers_count.' чел. <span style="color:green;">'.$count.'</span><br/>'.$model->type;
                        }
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'label'=>'Описание',
//                        'width' => '30%',
                        'content' => function($model){
                            $info = mb_substr($model->description,0,80,'UTF-8');
                            if ($model->ban) {
                                $info = $model->ban_info. ' - '.$model->ban_date.Html::a('Аккаунт - '.$model->join_id,
                                        ['/account/view?id='.$model->join_id],
                                        ['title'=>'Посмотреть', 'data-pjax' => '0',]);;
                            }
                            return $info;
                        }
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'Последний пост',
//                        'width' => '20%',
                        'content' => function($model){
                            $date_2 = date("Y-m-d H:i");
                            $interval_min = Settings::findByKey('interval_chanel')->value;;
                            $diff = strtotime($date_2) - strtotime($model->datetime_last_check) ;
                            $diff = round($interval_min - $diff /60);

                            $in = '';
                            $message = Message::find()->where(['text_id' => $model->last_post_id])->orderBy(['id' => SORT_DESC])->one();
                            if ($message) {
                                $in = "<br/><span style='font-size: 8px;color: #fd0c07;'> от {$message->datetime}</span><br/>";
                            }
                            return "<span style='color:darksalmon'> Обновиться через {$diff} минут</span><br/>".mb_substr($model->last_post_text,0,80,'UTF-8').'     '.$in;
                        }
                    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'subscribers_count',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'Подписчики',
//        'content' => function($model){
//            if($model->can_view_participants == 1){
//                return '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
//            } else if($model->can_view_participants == 0) {
//                return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
//            }
//        },
//        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
//        'width' => '5%',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'company_id',
//        'value' => 'company.name',
//        'visible' => Yii::$app->user->identity->isSuperAdmin(),
//    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'dropdown' => false,
                        'vAlign'=>'middle',
                        'urlCreator' => function($action, $model, $key, $index) {
                            return Url::to(['telegram-chanel/'.$action,'id'=>$key]);
                        },
                        'contentOptions' => ['style' => 'white-space: nowrap;'],
                        'template' => '{view}{assign-company} {delete} {update} {delete-relation}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fa fa-eye text-info" style="font-size: 16px;"></i>', $url, [
                                        'title'=>'Посмотреть', 'data-pjax' => '0',
                                    ])."&nbsp;";
                            },
                            'delete-relation' => function($url, $model) use ($isCommon){
                                if($isCommon == false){
                                    return Html::a('<i class="fa fa-arrow-right text-danger" style="font-size: 16px;"></i>', $url, [
                                        'role'=>'modal-remote', 'title'=>'Удалить',
                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                        'data-request-method'=>'post',
                                        'data-confirm-title'=>'Вы уверены?',
                                        'data-confirm-message'=>'Вы действительно хотите удалить канал из списка ваших каналов?'
                                    ]);
                                }
                            },
                            'assign-company' => function($url, $model) use ($isCommon){
                                if($isCommon){
                                    return Html::a('<i class="fa fa-arrow-left text-success" style="font-size: 16px;"></i>', $url, [
                                        'role'=>'modal-remote', 'title'=>'Привязать',
                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                        'data-request-method'=>'post',
                                        'data-confirm-title'=>'Вы уверены?',
                                        'data-confirm-message'=>'Вы действительно хотите привязать данный канал к вашей компании?'
                                    ]);
                                }
                            },
                            'delete' => function ($url, $model) {
                                if(Yii::$app->user->identity->isSuperAdmin()){
                                    return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                                        'role'=>'modal-remote', 'title'=>'Удалить',
                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                        'data-request-method'=>'post',
                                        'data-confirm-title'=>'Вы уверены?',
                                        'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                                    ]);
                                }
                            },
                            'update' => function ($url, $model) {
                                if(Yii::$app->user->identity->isSuperAdmin()){
                                    return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                                            'role'=>'modal-remote', 'title'=>'Изменить',
                                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                            'data-request-method'=>'post',
                                        ])."&nbsp;";
                                }
                            }
                        ],
                    ],
                ],
                'panelBeforeTemplate' => $panelBeforeTemplate,
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'headingOptions' => ['style' => 'display: none;'],
                    'after'=>$after,
                ]
            ])?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
