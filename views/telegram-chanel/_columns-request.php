<?php

use app\models\Key;
use app\models\TelegramChanelKey;
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'author',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'text',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'status',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'key_one_id',
        'content' => function($model){
            $text = '';

            if($model->keyOne){
                $text = $model->keyOne->value;
                $key = Key::findOne($model->keyOne->key_id);
                if ($key) {
                    $text = $key->name ." ({$model->keyOne->value}) ";
                }
            }

            return $text;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'value' => 'company.name',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'datetime',
    // ],

];