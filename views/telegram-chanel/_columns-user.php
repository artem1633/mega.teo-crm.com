<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Лого',
        'content' => function($model){
            if ($model->img == '') {
                $model->img = '/img/noimage.png';
            }
            return '<img src="'.$model->img.'" style="height: 35px; width: 35px; border-radius: 1em; border: 1px solid #fefefe; object-fit: cover;">';
        }
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Имя',
        'content' => function($model){
            if($model->is_new == 1){
                $a = '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
            } else if($model->is_new == 0) {
                $a = '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
            }
            return $model->name.' '. $a .'<br/>'.$model->login;
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'chanel_id',
//        'value' => 'chanel.name'
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'phone',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',
        'content' => function($model){
            return mb_substr($model->description,0,80,'UTF-8'); ;
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'company_id',
//        'value' => 'company.name',
//        'visible' => Yii::$app->user->identity->isSuperAdmin(),
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'updated_at',
//        'format' => ['date', 'php:d M Y H:i:s'],
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'created_at',
//        'format' => ['date', 'php:d M Y H:i:s'],
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'is_new',
//        'label' => 'Новый',
//        'content' => function($model){
//            if($model->is_new == 1){
//                return '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
//            } else if($model->is_new == 0) {
//                return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
//            }
//        },
//        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
//        'width' => '5%',
//    ],

];