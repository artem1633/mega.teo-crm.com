<?php

use app\models\Message;
use app\models\Settings;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Account */
?>


<div class="account-view">

    <div class="col-md-3">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Информация</h4>
            </div>
            <div class="panel-body" style="height: 700px; overflow-y: auto;">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'name',
                        'username',
                        'count_good',
                        'count_fuck',
                        'limit_24',
                        'numbur',
                        'api',
                        'hash',
                        'file_name',
                        'check',
                        'url_server:url',
                        'datetime_last_compare',
                        'datetime_last_check',
                        'created_at',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Каналы</h4>
            </div>
            <div class="panel-body" style="height: 700px; overflow-y: auto;">
                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $chanelDataProvider,
                    'filterModel' => $chanelSearchModel,
                    'pjax'=>true,
                    'columns' => [
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'label' => 'Лого',
                            'width' => '5%',
                            'content' => function($model){
                                return '<img src="'.$model->realImg.'" style="height: 50px; width: 50px; border-radius: 1em; border: 1px solid #fefefe; object-fit: cover;">';
                            }
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'label'=>'Канал',
                            'width' => '15%',
                            'content' => function($model){
                                $a = '';


                                $count = '';
                                if (!$model->username) {
                                    $url = $model->url;
                                } else {
                                    $url = $model->username;
                                }
                                $b = Html::a('@'.$url, 'tg://resolve?domain='.$url, [
                                    'title'=>'Перейти', 'blank' => '_target',
                                ]);
                                $dat = date('Y-m-d');
                                $mesAll = Message::find()->where(['like', 'datetime', $dat])->andWhere(['chanel_id' => $model->id])->count();

                                $link = Html::a(mb_substr($model->name,0,20,'UTF-8'),
                                    ['/telegram-chanel/view?id='.$model->id],
                                    ['title'=>'Посмотреть', 'data-pjax' => '0',]);
                                return $link.' '.$a.' <i class="fa fa-arrow-up text-success">'.$mesAll.'</i><br/>'.$b.' - '.$model->subscribers_count.' чел. <span style="color:green;">'.$count.'</span><br/>'.$model->type;
                            }
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'label'=>'Описание',
//                        'width' => '30%',
                            'content' => function($model){
                                return mb_substr($model->description,0,80,'UTF-8');
                            }
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'Последний пост',
//                        'width' => '20%',
                            'content' => function($model){
                                $date_2 = date("Y-m-d H:i");
                                $interval_min = Settings::findByKey('interval_chanel')->value;;
                                $diff = strtotime($date_2) - strtotime($model->datetime_last_check) ;
                                $diff = round($interval_min - $diff /60);

                                $in = '';
                                $message = Message::find()->where(['text_id' => $model->last_post_id])->orderBy(['id' => SORT_DESC])->one();
                                if ($message) {
                                    $in = "<br/><span style='font-size: 8px;color: #fd0c07;'> от {$message->datetime}</span><br/>";
                                }
                                return "<span style='color:darksalmon'> Обновиться через {$diff} минут</span><br/>".mb_substr($model->last_post_text,0,80,'UTF-8').'     '.$in;
                            }
                        ],
                    ]
                ])?>

            </div>
        </div>
    </div>

    <div class="col-md-9">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Логи</h4>
            </div>
            <div class="panel-body" style="height: 700px; overflow-y: auto;">
                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $logDataProvider,
                    'filterModel' => $logSearchModel,
                    'pjax'=>true,
                    'columns' => [
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'type',
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'text',
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'comment',
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'chanel_id',
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'datetime',
                        ],
                        // [
                        // 'class'=>'\kartik\grid\DataColumn',
                        // 'attribute'=>'account_id',
                        // ],
                    ]
                ])?>

            </div>
        </div>
    </div>
</div>
