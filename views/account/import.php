<?php

use yii\widgets\ActiveForm;

/** @var $model \app\models\forms\TelegramChanelImportForm */


?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<?= $form->field($model, 'file')->fileInput() ?>
<?= $form->field($model, 'id')->hiddenInput(['value' => $id])->label(false) ?>

<?php ActiveForm::end(); ?>
