<?php

use app\models\Log;
use app\models\Settings;
use app\models\TelegramChanel;
use yii\helpers\Html;
use yii\helpers\Url;



return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'company_id',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => '',
        'content' => function($model){
            $url = '';
            $url2 = '';
            $url3 = '';
            if (!$model->code_file) {
                $url = Html::a('Активир', 'http://194.67.112.94/action/hash.php?hash='.$model->hash.'&api='.$model->api, ['target' => '_blank', 'class' => 'btn btn-info']);
                $url2 = Html::a('Up файл', 'http://194.67.112.94/action/key/'.$model->hash, ['target' => '_blank', 'class' => 'btn btn-info']);
                $url3 = Html::a('Импорт', '/account/import?id='.$model->id, ['role' => 'modal-remote', 'class' => 'btn btn-info']);
            }
            return $url.'   '.$url2.'  '.$url3;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Инфо',
        'content' => function($model){
            $settingCount = Settings::findByKey('chanel_count_account')->value;
            $co = TelegramChanel::find()->where(['join_id' => $model->id])->count();
            $url3 = Html::a('<i class="fa fa-repeat text-danger" style="font-size: 15px;"></i>', "/account/ban?id={$model->id}", [
                'role'=>'modal-remote', 'title'=>'Проверить на бан',
                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                'data-request-method'=>'post',
            ]);
            $joinCount = Log::find()
                ->where(['like','datetime', date('Y-m-d')])
                ->andWhere(['account_id' => $model->id, 'type' => 'join'])
                ->count();
            return "
                    {$url3}
                     <span class='text-info'>КП: {$settingCount}<span/> /  
                     <span class='text-danger'>КФ: {$co}</span> /
                     <span class='text-info'> К за раз: {$model->count_send_time}</span> /
                     <span class='text-success'> Ин: {$model->interval}</span> /
                     <span class='text-success'> J: {$joinCount}</span> ";;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'username',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'count_good',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'count_fuck',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'numbur',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'code',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'api',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'hash',
    ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'file_name',
//     ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'check',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'url_server',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'datetime_last_compare',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'send_time',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['title'=>'View','data-toggle'=>'tooltip',  'data-pjax' => '0'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Are you sure?',
            'data-confirm-message'=>'Are you sure want to delete this item'],
    ],

];