<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Account */
?>
<div class="account-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
