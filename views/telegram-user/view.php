<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TelegramUser */
?>
<div class="telegram-user-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'chanel_id',
            'name',
            'login',
            'img',
            'description:ntext',
            'company_id',
            'is_new',
            'updated_at',
            'created_at',
        ],
    ]) ?>

</div>
