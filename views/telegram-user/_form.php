<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\TelegramChanel;
use app\models\Company;

/* @var $this yii\web\View */
/* @var $model app\models\TelegramUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="telegram-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'chanel_id')->widget(\kartik\select2\Select2::class, [
        'data' => ArrayHelper::map(TelegramChanel::find()->all(), 'id', 'name'),
    ]) ?>

    <div class="col-md-4">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
    </div>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_new')->checkbox() ?>

    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
        <?= $form->field($model, 'company_id')->widget(\kartik\select2\Select2::class, [
            'data' => ArrayHelper::map(Company::find()->all(), 'id', 'name'),
        ]) ?>
    <?php endif; ?>

    <div class="hidden">
        <?= $form->field($model, 'updated_at')->widget(\kartik\datetime\DateTimePicker::class, [

        ]) ?>

        <?= $form->field($model, 'created_at')->widget(\kartik\datetime\DateTimePicker::class, [

        ]) ?>

    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
