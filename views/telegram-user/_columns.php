<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Лого',
        'content' => function($model){
            if ($model->img == '') {
                $model->img = '/img/noimage.png';
            }
            return '<img src="'.$model->img.'" style="height: 50px; width: 50px; border-radius: 1em; border: 1px solid #fefefe; object-fit: cover;">';
        }
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Имя',
        'content' => function($model){
            if($model->is_new == 1){
                $a = '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
            } else if($model->is_new == 0) {
                $a = '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
            }
            return $model->name.' '. $a .'<br/>'.$model->login;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'chanel_id',
        'value' => 'chanel.name'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'phone',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',
        'content' => function($model){
            return mb_substr($model->description,0,80,'UTF-8'); ;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'value' => 'company.name',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'updated_at',
//         'format' => ['date', 'php:d M Y H:i:s'],
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'created_at',
//         'format' => ['date', 'php:d M Y H:i:s'],
//     ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'is_new',
//        'label' => 'Новый',
//        'content' => function($model){
//            if($model->is_new == 1){
//                return '<i class="fa fa-check text-success" style="font-size: 16px;"></i>';
//            } else if($model->is_new == 0) {
//                return '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
//            }
//        },
//        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
//        'width' => '5%',
//    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                        'role'=>'modal-remote', 'title'=>'Изменить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                    ])."&nbsp;";
            }
        ],
    ],

];   