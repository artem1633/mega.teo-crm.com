<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TelegramUser */
?>
<div class="telegram-user-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
