<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Company;
use app\models\PromoCode;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'balance')->input('number') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'partner_balance')->input('number') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'price_one_day')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'refer_id')->dropDownList(ArrayHelper::map(Company::find()->all(), 'id', 'name'), ['prompt' => 'Выберите компанию']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'promo_code_id')->dropDownList(ArrayHelper::map(PromoCode::find()->all(), 'id', 'name'), ['prompt' => 'Выберите промо код']) ?>
        </div>
    </div>

    <div class="hidden">
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'created_at')->widget(DateTimePicker::class, [

                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'last_activity_datetime')->widget(DateTimePicker::class, [

                ]) ?>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'access')->dropDownList([
                1 => 'Вкл',
                0 => 'Выкл',
            ]) ?>
        </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
