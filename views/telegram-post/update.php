<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TelegramPost */
?>
<div class="telegram-post-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
