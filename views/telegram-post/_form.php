<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TelegramPost */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="telegram-post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'chanel_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\TelegramChanel::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'chanel_href_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\TelegramChanel::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'img')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textInput() ?>

    <?= $form->field($model, 'datetime')->widget(DateTime) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
