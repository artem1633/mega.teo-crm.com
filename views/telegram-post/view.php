<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TelegramPost */
?>
<div class="telegram-post-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'chanel_id',
            'chanel_href_id',
            'img',
            'content',
            'datetime',
        ],
    ]) ?>

</div>
