<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TelegramPost */

?>
<div class="telegram-post-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
