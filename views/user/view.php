<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */
?>
<div class="user-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'password',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d M Y H:i:s'],
            ],
            'company.name',
            'is_deletable',
        ],
    ]) ?>

</div>
