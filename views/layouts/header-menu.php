<?php

use app\models\Request;
use app\models\Ticket;
use app\models\TicketMessage;


?>

<div id="sidebar" class="sidebar">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\Menu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Мои каналы', 'icon' => 'fa fa-telegram', 'url' => ['/telegram-chanel']],
                    ['label' => 'Баны', 'icon' => 'fa fa-telegram', 'url' => ['/telegram-chanel?ban=true']],
                    ['label' => 'Настройки', 'icon' => 'fa fa-cog', 'url' => '#', 'options' => ['class' => 'has-sub'], 'items' => [
                        ['label' => 'Аккаунты', 'url' => ['/account'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                        ['label' => 'Настройки системы', 'url' => ['/settings'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ]],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
