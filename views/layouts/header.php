<?php

use app\models\Log;
use yii\helpers\Html;
use app\models\Users;

?>

<div id="header" class="header navbar navbar-default navbar-fixed-top">
    <!-- begin container-fluid -->
    <div class="container-fluid">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
            <a href="<?=Yii::$app->homeUrl?>" class="navbar-brand" style="padding: 3px 20px !important;">


            </a>
            <button type="button" class="navbar-toggle" data-click="top-menu-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- end mobile sidebar expand / collapse button -->

        <?php if(Yii::$app->user->isGuest == false): ?>
            <!-- begin header navigation right -->
            <ul class="nav navbar-nav navbar-right">
                <li class="navbar-user" style="margin-right: 20px; margin-top: 19px;">
                    <?php
                    if(Yii::$app->user->identity->isSuperAdmin()){
                        $date = date('Y-m-d');
                        $new_postGood = intval(Log::find()
                            ->where(['like','datetime',$date])
                            ->andWhere(['type' => 'new_post'])
                            ->andWhere(['like','text','good'])->count());
                        $new_postError = intval(Log::find()
                            ->where(['like','datetime',$date])
                            ->andWhere(['type' => 'new_post'])
                            ->andWhere(['like','text','error'])->count());
                        $reportGood = intval(Log::find()
                            ->where(['like','datetime',$date])
                            ->andWhere(['type' => 'report'])
                            ->andWhere(['like','text','good'])->count());;
                        $reportError = intval(Log::find()
                            ->where(['like','datetime',$date])
                            ->andWhere(['type' => 'report'])
                            ->andWhere(['like','text','error'])->count());;
                        $infoGood = intval(Log::find()
                            ->where(['like','datetime',$date])
                            ->andWhere(['type' => 'info'])
                            ->andWhere(['like','text','good'])->count());;
                        $infoError = intval(Log::find()
                            ->where(['like','datetime',$date])
                            ->andWhere(['type' => 'info'])
                            ->andWhere(['like','text','error'])->count());;
                        $listGood = intval(Log::find()
                            ->where(['like','datetime',$date])
                            ->andWhere(['type' => 'list'])
                            ->andWhere(['like','text','good'])->count());
                        $listError = intval(Log::find()
                            ->where(['like','datetime',$date])
                            ->andWhere(['like','type','list'])
                            ->andWhere(['like','text','error'])->count());
                        $all = intval($new_postGood / 3 + $new_postError / 3 +
                            $reportError / 2 + $reportGood / 2 +
                            $infoError / 2 + $reportGood +
                            $listError / 4 + $listGood / 4);
                        echo "<h4 class=\"panel-title\">
                                <span class=\"text-info\">N:{$new_postGood}</span> /
                                <span class=\"text-danger\">{$new_postError}</span> /
                                <span class=\"text-info\">I: {$infoGood}</span> /
                                <span class=\"text-danger\">{$infoError}</span> /
                                <span class=\"text-info\"> R: {$reportGood}</span> /
                                <span class=\"text-danger\">{$reportError}</span>
                                <span class=\"text-info\"> U: {$listGood}</span> /
                                <span class=\"text-danger\">T{$all}</span>
                            </h4>";
                    }
                    ?>

                </li>
                <li class="navbar-user">

                    <p style="margin-top: 19px;"><?= Yii::$app->formatter->asCurrency(Yii::$app->user->identity->company->balance, 'rub') ?></p>
                </li>
                <li class="navbar-user">
                    <?= Html::a('<i class="fa fa-question-circle" style="font-size: 20px;"></i>', ['ticket/index'], ['title' => 'Тикеты', 'style' => 'cursor: pointer;']) ?>
                </li>
                <li class="dropdown navbar-user">
                    <a id="btn-dropdown_header" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/<?= Yii::$app->user->identity->getRealAvatarPath() ?>" data-role="avatar-view" alt="">
                        <span class="hidden-xs"><?=Yii::$app->user->identity->email?></span> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu animated fadeInLeft">
                        <li class="arrow"></li>
                        <li> <?= Html::a('Настройки пользователи', ['user/profile']) ?> </li>
                        <li> <?= Html::a('Тарифы', ['#']) ?> </li>
                        <li class="divider"></li>
                        <li> <?= Html::a('Выйти', ['/site/logout'], ['data-method' => 'post']) ?> </li>
                    </ul>
                </li>
            </ul>
            <!-- end header navigation right -->
        <?php endif; ?>
    </div>
    <!-- end container-fluid -->
</div>