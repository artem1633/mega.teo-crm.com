<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "telegram_user".
 *
 * @property int $id
 * @property int $chanel_id Канал
 * @property string $name Имя
 * @property string $login Логин
 * @property string $img Превью
 * @property string $description Описание
 * @property int $company_id Компания
 * @property int $is_new Новый(Да/Нет)
 * @property string $updated_at Дата и время обновления
 * @property string $created_at Дата и время добавления
 * @property string $phone Телефон
 * @property string $username Логин телеграмма
 *
 * @property TelegramChanel $chanel
 * @property Company $company
 */
class TelegramUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telegram_user';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => 'updated_at',
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s')
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'login'], 'required'],
            [['chanel_id', 'company_id', 'is_new'], 'integer'],
            [['description'], 'string'],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'login', 'img', 'phone', 'username'], 'string', 'max' => 255],
            [['chanel_id'], 'exist', 'skipOnError' => true, 'targetClass' => TelegramChanel::className(), 'targetAttribute' => ['chanel_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chanel_id' => 'Канал',
            'name' => 'Имя',
            'login' => 'Логин',
            'img' => 'Превью',
            'description' => 'Описание',
            'company_id' => 'Компания',
            'is_new' => 'Новый(Да/Нет)',
            'updated_at' => 'Дата и время обновления',
            'created_at' => 'Дата и время добавления',
            'phone' => 'Телефон',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChanel()
    {
        return $this->hasOne(TelegramChanel::className(), ['id' => 'chanel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
