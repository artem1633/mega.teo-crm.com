<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "account".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $username Ссылка
 * @property int $count_good Кол-во запросов удачных
 * @property int $count_fuck Кол-во запросов не удачных
 * @property string $numbur Номер
 * @property string $api API CODE
 * @property string $hash HASH
 * @property string $file_name Название файла
 * @property int $check Актиный
 * @property string $url_server URL сервера
 * @property string $code_file Код файла
 * @property string $datetime_last_compare Дата и время последнего совпадения
 * @property string $datetime_last_check Дата и время последней проверки
 * @property string $created_at
 * @property int $count_send_time Кол-во каналов за раз
 * @property int $interval Интервал отправки
 * @property int $send_status Статус отправки
 * @property string $send_time Время отправки последнего
 * @property int $limit_24 Лимит в сутки
 * @property string $status Статус
 */
class Account extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'account';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['count_good', 'count_fuck', 'check', 'count_send_time', 'interval', 'send_status', 'limit_24'], 'integer'],
            [['code_file'], 'string'],
            [['datetime_last_compare', 'datetime_last_check', 'created_at', 'send_time'], 'safe'],
            [['name', 'username', 'numbur', 'api', 'hash', 'file_name', 'url_server', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'username' => 'Ссылка',
            'count_good' => 'К удачных',
            'count_fuck' => 'К не удачных',
            'numbur' => 'Номер',
            'api' => 'API CODE',
            'hash' => 'HASH',
            'file_name' => 'Название файла',
            'check' => 'Актиный',
            'url_server' => 'URL сервера',
            'code_file' => 'Код файла',
            'datetime_last_compare' => 'Дата и время последнего совпадения',
            'datetime_last_check' => 'Дата и время последней проверки',
            'created_at' => 'Created At',
            'count_send_time' => 'Кол-во каналов за раз',
            'interval' => 'Интервал отправки',
            'send_status' => 'Статус отправки',
            'send_time' => 'Время последнего',
            'limit_24' => 'Лимит в сутки',
            'status' => 'Статус',
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->status = '';
            $this->check = 0;
            $this->created_at = date('Y-m-d H:i');
        }

        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        $chanels = TelegramChanel::find()->where(['join_id' => $this->id])->all();
        if ($chanels){
            foreach ($chanels as $chanel) {
                $chanel->join_id = null;
                $chanel->save(false);
            }
        }
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
