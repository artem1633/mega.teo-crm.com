<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telegram_post".
 *
 * @property int $id
 * @property int $chanel_id Канал
 * @property int $chanel_href_id Ссылка на канал
 * @property string $img Картинка
 * @property resource $content Текст
 * @property string $datetime Дата и время
 * @property integer $views Кол-во просмотров
 * @property integer $post_id ID поста
 *
 * @property TelegramChanel $chanelHref
 * @property TelegramChanel $chanel
 */
class TelegramPost extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'telegram_post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chanel_id', 'chanel_href_id', 'views', 'post_id'], 'integer'],
            [['content'], 'string'],
            [['datetime'], 'safe'],
            [['img'], 'string', 'max' => 255],
            [['chanel_href_id'], 'exist', 'skipOnError' => true, 'targetClass' => TelegramChanel::className(), 'targetAttribute' => ['chanel_href_id' => 'id']],
            [['chanel_id'], 'exist', 'skipOnError' => true, 'targetClass' => TelegramChanel::className(), 'targetAttribute' => ['chanel_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chanel_id' => 'Канал',
            'post_id' => 'ID поста',
            'chanel_href_id' => 'Ссылка на канал',
            'img' => 'Картинка',
            'content' => 'Текст',
            'datetime' => 'Дата и время',
            'views' => 'Кол-во просмотров',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChanelHref()
    {
        return $this->hasOne(TelegramChanel::className(), ['id' => 'chanel_href_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChanel()
    {
        return $this->hasOne(TelegramChanel::className(), ['id' => 'chanel_id']);
    }
}
