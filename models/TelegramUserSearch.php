<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TelegramUser;

/**
 * TelegramUserSearch represents the model behind the search form about `app\models\TelegramUser`.
 */
class TelegramUserSearch extends TelegramUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'chanel_id', 'company_id'], 'integer'],
            [['name', 'login', 'img', 'description', 'is_new', 'updated_at', 'created_at', 'phone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TelegramUser::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'chanel_id' => $this->chanel_id,
            'company_id' => $this->company_id,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'img', $this->img])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'is_new', $this->is_new]);

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $query->andWhere(['company_id' => Yii::$app->user->identity->company_id]);
        }

        return $dataProvider;
    }
}
