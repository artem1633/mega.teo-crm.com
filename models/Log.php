<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log".
 *
 * @property int $id
 * @property string $type Тип
 * @property string $text Текст
 * @property string $comment Комментарий
 * @property string $datetime Дата и время
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['comment'], 'string'],
            [['datetime'], 'safe'],
            [['type', 'text'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'text' => 'Текст',
            'comment' => 'Комментарий',
            'datetime' => 'Дата и время',
        ];
    }
}
