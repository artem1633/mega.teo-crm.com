<?php
namespace app\models\forms;

use app\models\Company;
use app\models\PromoCode;
use app\models\Settings;
use yii\base\Model;
use app\models\User;
use yii\behaviors\BlameableBehavior;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $userId;
    public $email;
    public $companyName;
    public $name;
    public $password;
    public $last_name;
    public $patronymic;
    public $phone;
    public $promo_code;
    public $confirm;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['companyName', 'email', 'password'], 'required'],
            ['email', 'trim'],
            ['email', 'email'],
            [['email', 'promo_code'], 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'targetAttribute' => 'email', 'message' => 'Этот email уже зарегистрирован',],
//            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Этот email уже зарегистрирован',
//                'when' => function($model, $attribute){
//                    if($this->userId != null)
//                    {
//                        $userModel = User::findOne($this->userId);
//                        return $this->{$attribute} !== $userModel->getOldAttribute($attribute);
//                    }
//                    return true;
//                },
//            ],
            ['confirm', function($model){
                if($this->confirm != 1){
                    $this->addError('confirm', 'Вы должны согласится с правилами');
                }
            }],
            ['password', 'string', 'min' => 6],
        ];
    }

    // public function scenarios()
    // {
    //     $scenarios = parent::scenarios();
    //     $scenarios['update'] = ['password', 'email'];//Scenario Values Only Accepted
    //     return $scenarios;
    // }

    public function attributeLabels()
    {
        return [
            'companyName' => 'ФИО',
            'password' => 'Пароль',
            'email' => 'Email',
//            'name' => 'ФИО',
            'phone' => 'Телефон',
            'promo_code' => 'Промо код',
            'confirm' => 'Я согласен с правилами использования'
        ];
    }

    /**
     * Signs user up.
     *
     * @return true
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $setting = Settings::findByKey('register_bonus');
        $settingPriceOneDay = Settings::findByKey('price_one_user');

        $startBalance = 0;
        $priceOneDay = 0;

        if($setting != null){
            $startBalance = $setting->value;
        }

        if($settingPriceOneDay != null){
            $priceOneDay = $settingPriceOneDay->value;
        }

        $promoCodeId = null;
        $referId = null;
        if($this->promo_code != null){
            $now = date('Y-m-d H:i:s');
            $promoCode = PromoCode::find()->where(['name' => $this->promo_code])->andWhere(['or', ['between', 'start_date', 'end_date', $now], ['endless' => 1]])->one();

            if($promoCode->action == PromoCode::ACTION_REGISTER_BONUS){
                $startBalance += $promoCode->action_value;
            }

            if($promoCode){
                $promoCodeId = $promoCode->id;
                $referId = $promoCode->company_id;
            }
        }


        $company = new Company([
            'name' => $this->companyName,
            'balance' => $startBalance,
            'promo_code_id' => $promoCodeId,
            'refer_id' => $referId,
            'price_one_day' => $priceOneDay,
        ]);
        $companySave = $company->save(false);


        $user = new User();
        $user->detachBehavior('company');
        $user->name = $this->companyName;
        $user->setPassword($this->password);
        $user->company_id = $company->id;
        $user->email = $this->email;
        $user->phone = $this->phone;
        $user->is_company_super_admin = 1;

        $userSave = $user->save(false);

        return ($companySave && $userSave);
    }

    /**
     * update user.
     *
     * @param User $user
     * @return User|null the saved model or null if saving fails
     */
    public function update($user)
    {
        if (!$this->validate()) {
            return null;
        }

        $user->name = $this->name;
        $user->surname = $this->surname;
        $user->patronymic = $this->patronymic;
        $user->phone = $this->phone;
        $user->category = $this->category;
        $user->department = $this->department;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->password = $this->password;
        // $user->generateAuthKey();
        
        return $user->update();
    }
}
