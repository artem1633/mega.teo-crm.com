<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 04.02.2020
 * Time: 16:36
 */

namespace app\models\forms;

use app\models\Account;
use app\models\TelegramChanel;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class TelegramChanelImportForm
 * @package app\models\forms
 */
class AccountKey extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;
    public $id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['file'], 'required'],
            ['file', 'file'],
            ['id', 'integer'],
        ];
    }

    /**
     * @return bool
     */
    public function import()
    {
        if($this->validate()){

            if(is_dir('key') == false){
                mkdir('key');
            }

            $this->file = UploadedFile::getInstance($this, 'file');


            if($this->id != null){
                $user = Account::findOne([$this->id]);
            } else {
                return false;
            }

            if($user->file_name != null){
                if(file_exists($user->file_name)){
                    unlink($user->file_name);
                }
            }

            $path = "key/{$user->hash}";
            $this->file->saveAs($path);
            $user->file_name = $path;
            $user->code_file = 'http://89.108.64.223/'.$path;
            $user->check = 1;
            $user->save(false);
            return true;

        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file' => 'Файл импорта',
            'id' => 'Id'
        ];
    }
}

//
//$model->file = UploadedFile::getInstance($model, 'file');
//if ($model->file) {
//
//    if ($model->upload()) {
//        if ($file = fopen($model->filename, "r")) {
//            $counter = 0;
//            while (!feof($file) && $counter < 5000) {
//                $line = fgets($file);
//                if ($line != null) {
//                    FunctionHelper::addRecipient($line, $model);
//                }
//                $counter++;
//            }
//            FunctionHelper::saveDataRecipient($model, $file);
//            fclose($file);
//            unlink($file);
//        }
//    }
//}