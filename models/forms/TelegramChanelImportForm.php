<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 04.02.2020
 * Time: 16:36
 */

namespace app\models\forms;

use app\models\TelegramChanel;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class TelegramChanelImportForm
 * @package app\models\forms
 */
class TelegramChanelImportForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['file'], 'required'],
            ['file', 'file']
        ];
    }

    /**
     * @return bool
     */
    public function import()
    {
        if($this->validate()){

            $this->file = UploadedFile::getInstance($this, 'file');

            if($this->file){
                $file = fopen($this->file->tempName, "r");

                $counter = 0;
                while (!feof($file) && $counter < 5000) {
                    $line = fgets($file);
                    if ($line != null) {

                        $telegramChanel = TelegramChanel::find()->where(['url' => $line])->one();

                        if($telegramChanel == null){
                            (new TelegramChanel(['url' => $line]))->save(false);
                        }
                    }
                    $counter++;
                }

                return true;
            }

            return false;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file' => 'Файл импорта'
        ];
    }
}

//
//$model->file = UploadedFile::getInstance($model, 'file');
//if ($model->file) {
//
//    if ($model->upload()) {
//        if ($file = fopen($model->filename, "r")) {
//            $counter = 0;
//            while (!feof($file) && $counter < 5000) {
//                $line = fgets($file);
//                if ($line != null) {
//                    FunctionHelper::addRecipient($line, $model);
//                }
//                $counter++;
//            }
//            FunctionHelper::saveDataRecipient($model, $file);
//            fclose($file);
//            unlink($file);
//        }
//    }
//}