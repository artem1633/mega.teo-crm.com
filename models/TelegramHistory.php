<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telegram_history".
 *
 * @property int $id
 * @property int $chanel_id Канал
 * @property int $grown Прирост
 * @property int $subscribers_count Кол-во подписчиков
 * @property int $posts_count Кол-во постов
 * @property string $last_post_id ID последнего поста
 * @property int $company_id Компания
 * @property string $created_at
 *
 * @property TelegramChanel $chanel
 * @property Company $company
 */
class TelegramHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telegram_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['chanel_id', 'subscribers_count', 'posts_count', 'company_id'], 'integer'],
            [['created_at'], 'safe'],
            [['last_post_id'], 'string', 'max' => 255],
            [['chanel_id'], 'exist', 'skipOnError' => true, 'targetClass' => TelegramChanel::className(), 'targetAttribute' => ['chanel_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chanel_id' => 'Канал',
            'subscribers_count' => 'Кол-во подписчиков',
            'grown' => 'Прирост',
            'posts_count' => 'Кол-во постов',
            'last_post_id' => 'ID последнего поста',
            'company_id' => 'Компания',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChanel()
    {
        return $this->hasOne(TelegramChanel::className(), ['id' => 'chanel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
