<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TelegramHistory;

/**
 * TelegramHistorySearch represents the model behind the search form about `app\models\TelegramHistory`.
 */
class TelegramHistorySearch extends TelegramHistory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'chanel_id', 'subscribers_count', 'posts_count', 'company_id'], 'integer'],
            [['last_post_id', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TelegramHistory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'chanel_id' => $this->chanel_id,
            'subscribers_count' => $this->subscribers_count,
            'posts_count' => $this->posts_count,
            'company_id' => $this->company_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'last_post_id', $this->last_post_id]);

        return $dataProvider;
    }
}
