<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "telegram_chanel".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $description Описание
 * @property string $url Ссылка
 * @property int $subscribers_count Кол-во подписчиков
 * @property int $count_posts_today Кол-во постов за сегодня
 * @property double $advertising_price Стоимость рекламы
 * @property int $last_post_id ID последнего поста
 * @property resource $last_post_text Текст поста
 * @property string $last_post_author Автор
 * @property int $growth Прирост
 * @property string $datetime_last_compare Дата и время последнего совпадения
 * @property string $datetime_last_check Дата и время последней проверки
 * @property string $created_at
 * @property string $img Превью канала
 * @property string $external_id Внещний ID канала
 * @property string $username Имя пользователя
 * @property int $can_view_participants Можно ли смотреть подписчиков
 * @property string $type Тип
 * @property int $join Вступили
 * @property int $join_id Кто вступил
 * @property int $send_true Разрешена отправка
 * @property int $send_bl Отправка произведена
 * @property string $send_time Дата и время отправки
 * @property int $ban Ban
 * @property string $ban_info Инфа о бане
 * @property string $ban_date Дата и время бана
 * @property string $join_date Дата и время вступления
 *
 * @property Message[] $messages
 * @property Request[] $requests
 * @property TelegramChanelBlack[] $telegramChanelBlacks
 * @property TelegramChanelCompany[] $telegramChanelCompanies
 * @property TelegramChanelKey[] $telegramChanelKeys
 * @property TelegramHistory[] $telegramHistories
 * @property TelegramUser[] $telegramUsers
 */
class TelegramChanel extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'telegram_chanel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['subscribers_count', 'growth', 'external_id', 'can_view_participants'], 'integer'],
            [['advertising_price'], 'number'],
            [['created_at', 'img'], 'safe'],
            ['url', 'trim'],
            [['name', 'url', 'username', 'type'], 'string', 'max' => 255],
//            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'img' => 'Превью',
            'external_id' => 'Внешний ID',
            'description' => 'Описание',
            'url' => 'Ссылка',
            'subscribers_count' => 'Кол-во подписчиков',
            'count_posts_today' => 'Кол-во постов за сегодня',
            'advertising_price' => 'Стоимость рекламы',
            'last_post_id' => 'ID последнего поста',
            'last_post_text' => 'Текст поста',
            'last_post_author' => 'Автор',
            'growth' => 'Прирост',
            'datetime_last_compare' => 'Дата и время последнего совпадения',
            'datetime_last_check' => 'Дата и время последней проверки',
            'created_at' => 'Created At',
            'can_view_participants' => 'Можно ли смотреть подписчиков',
            'username' => 'Имя пользователя',
            'type' => 'Тип',
            'join' => 'Вступили',
            'join_id' => 'Кто вступил',
            'send_true' => 'Разрешена отправка',
            'send_bl' => 'Отправка произведена',
            'send_time' => 'Дата и время отправки',
            'ban' => 'Ban',
            'ban_info' => 'Инфа о бане',
            'ban_date' => 'Дата и время бана',
            'join_date' => 'Дата и время вступления',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $this->url = trim($this->url);
        if($this->isNewRecord){
            $telegram = TelegramChanel::find()->where(['url' => $this->url])->one();
            if($telegram){

                return false;
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

    }

    /**
     * @return string
     */
    public function getRealImg()
    {
        if($this->img == null){
            return '/img/noimage.png';
        }

        return $this->img;
    }

    /**
     * @return Company[]|static[]
     */
    public function getCompanies()
    {
        $companiesPks = ArrayHelper::getColumn(TelegramChanelCompany::find()->where(['telegram_chanel_id' => $this->id])->all(), 'company_id');
        return Company::findAll($companiesPks);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelegramPosts()
    {
        return $this->hasMany(TelegramPost::className(), ['chanel_href_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelegramPosts0()
    {
        return $this->hasMany(TelegramPost::className(), ['chanel_id' => 'id']);
    }
}
