<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $name ФИО
 * @property string $phone Телефон
 * @property int $refer_id Кто пригласил
 * @property int $is_super_company Является ли супер компанией
 * @property int $access Доступ (вкл/выкл)
 * @property string $last_activity_datetime Дата и время последней активности
 * @property double $balance Баланс
 * @property double $partner_balance Партнерский баланс
 * @property double $price_one_day Стоимость одного дня
 * @property int $promo_code_id Промо код
 * @property string $created_at
 *
 * @property PromoCode $promoCode
 * @property Company $refer
 * @property Company[] $companies
 * @property TelegramChanel[] $telegramChannels
 * @property Key[] $keys
 * @property Request[] $requests
 * @property User[] $users
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['refer_id', 'is_super_company', 'access', 'promo_code_id'], 'integer'],
            [['last_activity_datetime', 'created_at'], 'safe'],
            [['balance', 'partner_balance', 'price_one_day'], 'number'],
            [['name', 'phone'], 'string', 'max' => 255],
            [['promo_code_id'], 'exist', 'skipOnError' => true, 'targetClass' => PromoCode::className(), 'targetAttribute' => ['promo_code_id' => 'id']],
            [['refer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['refer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'phone' => 'Телефон',
            'refer_id' => 'Кто пригласил',
            'is_super_company' => 'Является ли супер компанией',
            'access' => 'Доступ (вкл/выкл)',
            'last_activity_datetime' => 'Дата и время последней активности',
            'balance' => 'Баланс',
            'partner_balance' => 'Партнерский баланс',
            'promo_code_id' => 'Промо код',
            'created_at' => 'Дата и время регистрации',
            'price_one_day' => 'Стоимость одного дня'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoCode()
    {
        return $this->hasOne(PromoCode::className(), ['id' => 'promo_code_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefer()
    {
        return $this->hasOne(Company::className(), ['id' => 'refer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['refer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeys()
    {
        return $this->hasMany(Key::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Request::className(), ['company_id' => 'id']);
    }

    /**
     * @return TelegramChanel[]|static[]
     */
    public function getTelegramChannels()
    {
        $channelsPks = ArrayHelper::getColumn(TelegramChanelCompany::find()->where(['company_id' => $this->id])->all(), 'telegram_chanel_id');
        return TelegramChanel::findAll($channelsPks);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['company_id' => 'id']);
    }
}
