<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company_setting".
 *
 * @property int $id
 * @property int $company_id Компания
 * @property string $bitrix_user_identity ID пользователя Битрикс24
 * @property string $bitrix_webhook Вебхук Битрикс24
 *
 * @property Company $company
 */
class CompanySetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'integer'],
            [['bitrix_user_identity', 'bitrix_webhook'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'bitrix_user_identity' => 'ID пользователя Битрикс24',
            'bitrix_webhook' => 'Вебхук Битрикс24',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
