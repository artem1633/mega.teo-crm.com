<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TelegramChanel;

/**
 * TelegramChanelSearch represents the model behind the search form about `app\models\TelegramChanel`.
 */
class TelegramChanelSearch extends TelegramChanel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'subscribers_count', 'growth'], 'integer'],
            [['img', 'verify_mark', 'discount_mark', 'name', 'url', 'section', 'created_at', 'username', 'can_view_participants', 'description', 'type'], 'safe'],
            [['advertising_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TelegramChanel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'datetime_last_check' => SORT_ASC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'subscribers_count' => $this->subscribers_count,
            'advertising_price' => $this->advertising_price,
            'growth' => $this->growth,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['or', ['like', 'name', $this->name], ['like', 'description', $this->name]])
            ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
