<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "message".
 *
 * @property int $id
 * @property int $chanel_id Канал
 * @property resource $text Текст
 * @property string $text_id ID текста
 * @property string $datetime Дата и время
 * @property int $company_id Компания
 *
 * @property TelegramChanel $chanel
 * @property Company $company
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['chanel_id', 'company_id'], 'integer'],
            [['text'], 'string'],
            [['datetime'], 'safe'],
            [['text_id'], 'string', 'max' => 255],
            [['chanel_id'], 'exist', 'skipOnError' => true, 'targetClass' => TelegramChanel::className(), 'targetAttribute' => ['chanel_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chanel_id' => 'Канал',
            'text' => 'Текст',
            'text_id' => 'ID текста',
            'datetime' => 'Дата и время',
            'company_id' => 'Компания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChanel()
    {
        return $this->hasOne(TelegramChanel::className(), ['id' => 'chanel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
