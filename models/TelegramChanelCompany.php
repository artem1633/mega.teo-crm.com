<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telegram_chanel_company".
 *
 * @property int $id
 * @property int $telegram_chanel_id
 * @property int $company_id
 *
 * @property Company $company
 * @property TelegramChanel $telegramChanel
 */
class TelegramChanelCompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telegram_chanel_company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telegram_chanel_id', 'company_id'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['telegram_chanel_id'], 'exist', 'skipOnError' => true, 'targetClass' => TelegramChanel::className(), 'targetAttribute' => ['telegram_chanel_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telegram_chanel_id' => 'Telegram Chanel ID',
            'company_id' => 'Company ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelegramChanel()
    {
        return $this->hasOne(TelegramChanel::className(), ['id' => 'telegram_chanel_id']);
    }
}
